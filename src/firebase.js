import { initializeApp } from "firebase/app";
import { getDatabase } from "firebase/database";

const firebaseConfig = {
	apiKey: 'AIzaSyCL_ZCJFBB7lbel-jsV6cY4uVSiy-Z_aFY',
	authDomain: 'eygo-doc.firebaseapp.com',
	projectId: 'eygo-doc',
	storageBucket: 'eygo-doc.appspot.com',
	messagingSenderId: '735048474493',
	appId: '1:735048474493:web:1eee749e9f3bbfb8c9b614',
	measurementId: 'G-00T5X4QNSX',
	databaseURL: 'https://eygo-doc-default-rtdb.europe-west1.firebasedatabase.app'

	// apiKey: process.env.REACT_APP_FIREBASE_API_KEY,
	// authDomain: process.env.REACT_APP_FIREBASE_AUTH_DOMAIN,
	// projectId: process.env.REACT_APP_FIREBASE_PROJECT_ID,
	// storageBucket: process.env.REACT_APP_FIREBASE_STORAGE_BUCKET,
	// messagingSenderId: process.env.REACT_APP_FIREBASE_MESSAGING_SENDER_ID,
	// appId: process.env.REACT_APP_FIREBASE_APP_ID,
	// measurementId: process.env.REACT_APP_FIREBASE_MEASUREMENT_ID
};

const app = initializeApp(firebaseConfig);

export const database = getDatabase(app);