import { userSelector } from "../redux/user/selectors";
import { useEffect } from "react";
import { useSelector } from "react-redux";
import { setUser } from "../redux/user/slice";
import { useAppDispatch } from "src/redux/store";

const useAuth = () => {
  const dispatch = useAppDispatch();
  const userData = window.localStorage.getItem("userData");
  const { email, documentsList, paymentTerm, firmData, retailers, token, id } =
    useSelector(userSelector);

  useEffect(() => {
    dispatch(setUser(JSON.parse(userData)));
  }, []);

  return {
    isAuth: !!email,
    email,
    documentsList,
    paymentTerm,
    firmData,
    retailers,
    token,
    id,
  };
};

export default useAuth;
