import React from "react";
import { Route, Routes } from "react-router-dom";
import { Home, Contract, CheckCompany } from "./pages";
import "./styles/App.scss";
import "./styles/icons-font.scss";
import PrivateRoute from "./hoc/PrivateRoute";
import MainLayout from "./layouts/MainLayout";

function App() {
  React.useEffect(() => {
    document.title = "DocuMate";
  }, []);

  return (
    <Routes>
      <Route path="/" element={<MainLayout />}>
        <Route path="" element={<Home />} />

        <Route
          path="check-company"
          element={
            <PrivateRoute>
              <CheckCompany />
              {/* <Contract /> */}
            </PrivateRoute>
          }
        />

        <Route
          path="contract"
          element={
            <PrivateRoute>
              <Contract />
            </PrivateRoute>
          }
        />
      </Route>
    </Routes>
  );
}

export default App;
