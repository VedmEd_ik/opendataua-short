export type dataFromFormType = {
  status?: boolean;
  // Загальні дані для договору (номер, дата тощо)
  documentName: string;
  documentNumber: string;
  documentDate: string;
  documentPayment: string;
  documentFontSize: number;
  manager: string;
  retailer: string;

  // Дані для договору про контрагента (підписант, паспорт, податкова інформація тощо)
  signer: {
    name: string;
    role: string;
  };
  signerBasisDocument: string;
  postAddress: string;
  passport: {
    status: boolean;
    number: string;
    date: string;
    organName: string;
    organRegion: string;
    passportType: string;
  };
  phones: string;
  email: string;
  phonesAdd: string;
  emailAdd: string;
  bankDetails: {
    bank1: {
      "bank-account": string;
      bank: string;
    };
    bank2: {
      "bank-account": string;
      bank: string;
    };
  };
  isTax: boolean;
  taxationSystem: string;
  PDV: string;
  IPN: string;
};

export interface DocumentSliceState {
  documentData: dataFromFormType;
}
