import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { DocumentSliceState } from "../document/types";

const initialState: DocumentSliceState = {
  documentData: {
    status: false,
    documentName: "",
    documentNumber: "",
    documentDate: "",
    documentPayment: "",
    documentFontSize: 9,
    manager: "",
    retailer: "",

    // Дані для договору про контрагента (підписант, паспорт, податкова інформація тощо)
    signer: {
      name: "",
      role: "",
    },
    signerBasisDocument: "",
    postAddress: "",
    passport: {
      status: false,
      number: "",
      date: "",
      organName: "",
      organRegion: "",
      passportType: "",
    },
    phones: "",
    email: "",
    phonesAdd: "",
    emailAdd: "",
    bankDetails: {
      bank1: {
        "bank-account": "",
        bank: "",
      },
      bank2: {
        "bank-account": "",
        bank: "",
      },
    },
    isTax: false,
    taxationSystem: "",
    PDV: "",
    IPN: "",
  },
};

export const documentSlice = createSlice({
  name: "document",
  initialState,
  reducers: {
    setDocumentData(state, action: PayloadAction<DocumentSliceState["documentData"]>) {
      state.documentData = { ...state.documentData, ...action.payload };
    },

    removeDocumentData(state) {
      state.documentData = initialState.documentData;
    },

    setStatusDocument(state) {
      if (
        state.documentData.documentName &&
        state.documentData.documentNumber &&
        state.documentData.documentDate &&
        state.documentData.documentPayment
      ) {
        state.documentData.status = true;
      } else {
        state.documentData.status = false;
      }
    },
  },
});

export const { setStatusDocument, setDocumentData, removeDocumentData } = documentSlice.actions;

export default documentSlice.reducer;
