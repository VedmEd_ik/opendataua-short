import { RootState } from "../store";

export const statusDocumentSelector = (state: RootState) => state.documentSlice.documentData.status;

export const nameDocumentSelector = (state: RootState) =>
  state.documentSlice.documentData.documentName;

export const fontSizeSelector = (state: RootState) =>
  state.documentSlice.documentData.documentFontSize;

export const paymentTermSelector = (state: RootState) =>
  state.documentSlice.documentData.documentPayment;

export const documentDataSelector = (state: RootState) => state.documentSlice.documentData;
