import { RootState } from "../store";

export const userSelector = (state: RootState) => state.userSlice.user;
export const documentsListSelector = (state: RootState) => state.userSlice.user.documentsList;
export const firmDataSelector = (state: RootState) => state.userSlice.user.firmData;
export const paymentTermSelector = (state: RootState) => state.userSlice.user.paymentTerm;
export const managersSelector = (state: RootState) => state.userSlice.user.managers;
export const retailersSelector = (state: RootState) => state.userSlice.user.retailers;
