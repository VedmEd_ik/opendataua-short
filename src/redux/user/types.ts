import {
  DocumentsListType,
  FirmDataType,
  ManagersType,
  PaymentTermType,
  RetailersType,
} from "src/@types/types";

export interface UserSliceState {
  user: UserType;
}

export type UserType = {
  id: string;
  name: string;
  email: string;
  token?: string;
  documentsList: DocumentsListType;
  paymentTerm: PaymentTermType;
  firmData?: FirmDataType;
  managers?: ManagersType;
  retailers?: RetailersType;
};
