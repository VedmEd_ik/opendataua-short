import { configureStore } from "@reduxjs/toolkit";
import { useDispatch } from "react-redux";
import counterPartySlice from "./counterparty/slice";
import documentSlice from "./document/slice";
import myCompanySlice from "./myCompany/slice";
import userSlice from "./user/slice";

export const store = configureStore({
  reducer: {
    counterPartySlice,
    myCompanySlice,
    documentSlice,
    userSlice,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
export const useAppDispatch = () => useDispatch<AppDispatch>();
