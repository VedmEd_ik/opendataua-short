export type BankDetailsType = {
  bank1?: { "bank-account"?: string; bank?: string };
  bank2?: { "bank-account"?: string; bank?: string };
};

export type PassportType = {
  status?: string;
  number?: string;
  date?: string;
  organName?: string;
  organRegion?: string;
  passportType?: string;
};

type StatusRequesType = "loading" | "success" | "error" | boolean;

export interface CounerpartySliceState {
  status: StatusRequesType;
  errorCode: boolean;
  counterParty: NewCounterPartyType;
  extractEDRLink: string;
  statusExtractEDR: StatusRequesType;
}

type AddressType = {
  zip?: string;
  country?: string;
  address?: string;
  parts?: {
    atu?: string;
    street?: string;
    house?: string;
    building?: string;
    num?: string;
    houseType?: string;
    numType?: string;
    atuCode?: string;
  };
};

export type FactorItemsType = {
  number?: string;
  courtName?: string;
  category?: string;
  department?: string;
  type?: string;
  text?: string;
  count?: number;
  link?: string;
  liveCount?: number;
  decisionNumber?: string;
  decisionDate?: string;
  agency?: string;
  date?: string;
  court?: string;
  regAction?: string;
  members?: string;
  liquidatorName?: string;
  reorganizationType?: string;
  requirementDate?: string;
  pib?: string;
  declarantId?: string;
  years?: number[];
  changes?: {
    field?: string;
    record?: string;
    text?: string;
    oldValue?: string;
    newValue?: string;
  }[];
};

export type FactorType = {
  factorGroup?: string;
  type?: string;
  text?: string;
  icon?: string;
  indicator?: string;
  number?: string;
  status?: string;
  dateCancellation?: string;
  reasonCancellation?: string;
  agencyCancellation?: string;
  databaseDate?: string;
  dateEnd?: string;
  group?: number;
  rate?: string;
  total?: number;
  local?: number;
  government?: number;
  debt?: number;
  penaltiesCount?: string;
  link?: string;
  count?: number;
  deadCounter?: number;
  startDate?: string;
  dateStart?: string;
  endDate?: string;
  termless?: boolean;
  sanctionList?: string;
  sanctionReason?: string;
  sanctionComment?: string;
  ctottgCode?: string;
  occupationStatus?: boolean;
  blockadeStatus?: boolean;
  fightStatus?: boolean;
  evacuationStatus?: boolean;
  liberatedStatus?: boolean;
  excludedStatus?: boolean;
  updatedAt?: string;
  items?: FactorItemsType[];
};

export type DataFromAxios = {
  registry?: {
    fullName?: string;
    shortName?: string;
    fullNameEn?: string;
    shortNameEn?: string;
    code?: string;
    location?: string;
    email?: string;
    primaryActivity?: string;
    status?: string;
    ceoName?: string;
    phones?: string[];
    registrationDate?: string;
    capital?: number;
    lastTime?: string;
    pdv_status?: string;
    pdv_code?: string;
    heads?: {
      name?: string;
      role?: string;
      restriction?: string;
    }[];
    registrations?: {
      code?: string;
      name?: string;
      type?: number;
      startDate?: string;
      description?: string;
      endDate?: string;
    }[];
    beneficiaries?: {
      name?: string;
      role?: string;
      amount?: number;
      code?: number;
      location?: string;
      country?: string;
      amountPercent?: number;
    }[];
    fax?: string;
    webPage?: string;
    anotherInfo?: string;
    includeOlf?: number;
    state?: string;
    olfCode?: string;
    olfName?: string;
    olfSubtype?: string;
    foundingDocumentType?: number;
    foundingDocumentName?: string;
    executivePower?: {
      name?: string;
      code?: string;
    }[];
    objectName?: string;
    branches?: {
      name?: string;
      code?: string;
      address?: AddressType;
      id?: number;
      role?: number;
      roleText?: string;
    }[];
    authorisedCapital?: {
      value?: number;
      date?: string;
    };
    management?: string;
    managingPaper?: string;
    activities?: {
      code?: string;
      name?: string;
      isPrimary?: boolean;
    }[];
    address?: AddressType;
    registration?: {
      date?: string;
      recordNumber?: string;
      recordDate?: string;
      isSeparation?: boolean;
      isDivision?: boolean;
      isMerge?: boolean;
      isTransformation?: boolean;
    };
    bankruptcy?: {
      date?: string;
      state?: number;
      dateJudge?: string;
      stateText?: string;
      docDate?: string;
      docNumber?: string;
      courtName?: string;
    };
    termination?: {
      state?: number;
      stateText?: string;
      date?: string;
      recordNumber?: string;
      cause?: string;
    };
    terminationCancel?: {
      date?: string;
      recordNumber?: string;
      docDate?: string;
      courtName?: string;
      docNumber?: string;
      dateJudge?: string;
    };
    assignees?: {
      name?: string;
      code?: string;
      address?: AddressType;
    }[];
    predecessors?: {
      name?: string;
      code?: string;
      address?: AddressType;
    }[];
    primaryActivityKind?: {
      name?: string;
      code?: string;
      class?: string;
      regNumber?: string;
    };
    prevRegistrationEndTerm?: string;
    openEnforcements?: string[];
  };
  factors?: FactorType[];
  financialStatement?: {
    year?: number;
    revenue?: number;
    expenses?: number;
    profit?: number;
    nonCurrentAssets?: number;
    currentAssets?: number;
    liability?: number;
    balance?: number;
    sector?: string;
    buhgalter?: string;
  }[];
  registries?: {
    prozorro?: {
      name?: string;
      link?: string;
      count?: number;
      tenders?: {
        id?: string;
        registry?: string;
        title?: string;
        amount?: number;
        role?: string;
        buyer?: {
          code?: string;
          name?: string;
          legalName?: string;
          contactName?: string;
          contactEmail?: string;
          contactPhone?: string;
        }[];
        seller?: {
          name?: string;
          code?: string;
          contactName?: string;
          contactPhone?: string;
          contactEmail?: string;
        }[];
        status?: string;
        description?: string;
        updatedAt?: string;
        createdAt?: string;
        startDate?: string;
      }[];
    };
    smida?: {
      name?: string;
      text?: string;
      date?: string;
      year?: number;
      quarter?: number;
      shareholders?: {
        fullName?: string;
        code?: string;
        role?: string;
        country?: string;
        amountPercent?: number;
      }[];
    };
    licenses?: {
      gamingLicenses?: {
        name?: string;
        text?: string;
        licenseType?: string;
        licenseSubType?: string;
        date?: string;
        licenseNumber?: number;
        brand?: string;
        address?: string;
      }[];
      bigTaxpayers?: {
        name?: string;
        text?: string;
        year?: number;
      }[];
      diiaCity?: {
        name?: string;
        text?: string;
        decisionDate?: string;
        number?: number;
      }[];
    };
  };
  dpa?: {
    taxDepartments?: {
      taxDepartmentId?: number;
      regionCode?: number;
      districtCode?: number;
      districtName?: string;
      inspectionCode?: number;
      inspectionName?: string;
      inspectionRegistryCode?: number;
      code?: number;
      koatuuCode?: string;
      regionTaxDepartmentCode?: number;
    };
    taxRequisites?: [
      {
        type?: string;
        koatuuObl?: string;
        koatuu?: string;
        location?: string;
        recipient?: string;
        code?: number;
        bank?: string;
        mfo?: number;
        iban?: string;
        taxCode?: number;
      },
    ];
  };
  courtEntity?: {
    courtId?: number;
    name?: string;
    code?: string;
    stage?: string;
    type?: string;
  };
  link?: string;
};

export interface AxiosResponseNew {
  data?: {
    status?: string;
    forDevelopers?: string;
    data?: AxiosResponseFOP | AxiosResponseCompanyNew;
  };
}

export type FactorTypeNew = {
  factorGroup: string;
  type: string;
  text: string;
  icon: string;
  indicator: string;
  number: string;
  status: string;
  dateCancellation: string;
  reasonCancellation: string;
  agencyCancellation: string;
  databaseDate: string;
  dateEnd: string;
  group: number;
  rate: string;
  total: number;
  local: number;
  government: number;
  probablyFightStatus: boolean;
  activeFightStatus: boolean;
  tot: boolean;
  activeRegistry: boolean;
  items: {
    date: string;
    changes: {
      field: string;
      record: string;
      text: string;
      oldValue: string;
      newValue: string;
    }[];
  }[];
};

export type AxiosResponseFOP = {
  registry?: {
    activities?: {
      name: string;
      code: string;
      isPrimary: boolean;
    }[];
    registrations?: {
      name: string;
      code: string;
      description: string;
      type: string;
      startNum: string;
      startDate: string;
      endNum: string;
      endDate: string;
    }[];
    address?: {
      zip: string;
      country: string;
      address: string;
      parts: {
        atu: string;
        street: string;
        house: string;
        building: string;
        num: string;
        houseType: string;
        numType: string;
        atuCode: string;
      };
    };
    termination?: {
      state: number;
      stateText: string;
      date: string;
      recordNumber: string;
      cause: string;
    };
    terminationCancel?: {
      date: string;
      recordNumber: string;
      docDate: string;
      courtName: string;
      docNumber: string;
      dateJudge: string;
    };
    bankruptcy?: {
      date: string;
      state: number;
      dateJudge: string;
      stateText: string;
      docDate: string;
      docNumber: string;
      courtName: string;
    };
    primaryActivity?: string;
    registration?: {
      date: string;
      recordNumber: string;
      recordDate: string;
      isSeparation: boolean;
      isMerge: boolean;
      isTransformation: boolean;
      isDivision: boolean;
    };
    sex?: string;
    objectName?: string;
    code?: number;
    location?: string;
    country?: string;
    status?: string;
    email?: string;
    phones?: string[];
    phonesFormatted?: string[];
    hash?: string;
    lastDate?: string;
    birthDate?: string;
    fullName?: string;
    additionallyActivities?: string[];
    registrationDate?: string;
    registrationNumber?: string;
  };
  factors?: FactorTypeNew[];
  dpa?: {
    taxDepartments?: {
      taxDepartmentId?: number;
      regionCode?: number;
      districtCode?: number;
      districtName?: string;
      inspectionCode?: number;
      inspectionName?: string;
      inspectionRegistryCode?: number;
      code?: 0;
      koatuuCode?: string;
      regionTaxDepartmentCode?: number;
    };
    taxRequisites?: [
      {
        type: string;
        koatuuObl: string;
        koatuu: string;
        location: string;
        recipient: string;
        code: number;
        bank: string;
        mfo: number;
        iban: string;
        taxCode: number;
      },
    ];
  };
};

export type AxiosResponseCompanyNew = {
  full_name?: string;
  short_name?: string;
  code?: string;
  ceo_name?: string;
  location?: string;
  activities?: string;
  status?: string;
  beneficiaries?: {
    title?: string;
    location?: string;
    capital?: number;
    role?: string;
  }[];
  database_date?: string;
  pdv_code?: string;
  pdv_status?: string;
  pdv_database_date?: string;
};

export type NewCounterPartyType = {
  // =========== Основна обов'язкова інформація для договору ===========
  fullName: string;
  shortName: string;
  code: string;
  address: string;
  ceo: string;
  phones?: string[];
  email?: string;

  // =========== Додаткова не обов'зякова інформація для договору ===========
  pdvStatus?: string;
  pdvCode?: string;

  // =========== Додаткова інформація для відображення на сторінці CheckCompany ===========
  status: string;
  primaryActivity?: string;
  activities?: {
    name: string;
    code: string;
    isPrimary: boolean;
  }[];
  bankruptcy?: string;
  beneficiaries?: {
    title?: string;
    capital?: number;
    location?: string;
    role?: string;
  }[];
  pdvStatusIcon?: string;
  pdvText?: string;

  singleTaxStatus?: string;
  singleTaxIcon?: string;
  singleTaxText?: string;
  singleTaxGroup?: string;

  taxDebtText?: string;
  taxDebtIcon?: string;

  warTerritoryStatus?: string;
  warTerritoryText?: string;
  warTerritoryIcon?: string;

  isFOP: boolean;
};
