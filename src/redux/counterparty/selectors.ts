import { RootState } from "../store";

export const counterPartySelector = (state: RootState) => state.counterPartySlice.counterParty;

export const statusCounterPartySelector = (state: RootState) => state.counterPartySlice.status;

export const counterPartyCodeSelector = (state: RootState) =>
  state.counterPartySlice.counterParty.code;

export const extractEDRSelector = (state: RootState) => state.counterPartySlice.extractEDRLink;

export const statusExtractEDRSelector = (state: RootState) =>
  state.counterPartySlice.statusExtractEDR;
