import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { CounerpartySliceState, NewCounterPartyType } from "../counterparty/types";
import { fetchCompany } from "./fetchCompanyAsyncThunks";

const initialState: CounerpartySliceState = {
  status: null,
  errorCode: null,
  counterParty: {
    isFOP: null,
    status: "",
    fullName: "",
    shortName: "",
    code: "",
    address: "",
    ceo: "",
    phones: null,
    email: null,

    pdvStatus: "",
    pdvCode: "",

    bankruptcy: "",
    beneficiaries: null,
    pdvStatusIcon: "",

    singleTaxStatus: null,
    singleTaxIcon: null,
    singleTaxText: null,
    singleTaxGroup: null,

    taxDebtText: null,
    taxDebtIcon: null,

    warTerritoryStatus: null,
    warTerritoryText: null,
    warTerritoryIcon: null,
  },
  extractEDRLink: null,
  statusExtractEDR: null,
};

export const counterPartySlice = createSlice({
  name: "party",
  initialState,
  reducers: {
    setCounterParty(state, action: PayloadAction<NewCounterPartyType>) {
      state.counterParty = { ...state.counterParty, ...action.payload };
    },
    removeCounterparty(state) {
      state.counterParty = initialState.counterParty;
      state.status = false;
    },
    clearCounterparty(state) {
      state.counterParty = initialState.counterParty;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(fetchCompany.pending, (state) => {
      console.log("Відправка запиту");
      state.status = "loading";
      state.counterParty = null;
      state.extractEDRLink = null;
    });
    builder.addCase(fetchCompany.fulfilled, (state, action) => {
      console.log("Відповідь від сервера отримано");
      state.counterParty = action.payload;
      state.status = "success";
    });
    builder.addCase(fetchCompany.rejected, (state) => {
      console.log("Помилка сервера");
      state.status = "error";
      state.errorCode = false;
      state.counterParty = null;
      state.extractEDRLink = null;
    });
  },
});

export const { setCounterParty, removeCounterparty, clearCounterparty } = counterPartySlice.actions;

export default counterPartySlice.reducer;
