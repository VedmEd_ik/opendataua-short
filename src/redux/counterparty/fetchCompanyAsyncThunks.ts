import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import { checkCode, transformDataFromFetch } from "src/utils/functions";
import { NewCounterPartyType } from "./types";

// Axios запит на сервер для отримання інформації про компанію
export const fetchCompany = createAsyncThunk<NewCounterPartyType, string>(
  "party/fetchCompanyStatus",
  async (companyCode) => {
    let response;
    if (checkCode(companyCode) === "Company") {
      try {
        const res = await axios.get(
          `https://opendatabot.com/api/v2/company/${companyCode}?apiKey=${process.env.REACT_APP_API_KEY_OPENDATA}`,
        );
        console.log(res);
        if (res.status === 200) {
          response = transformDataFromFetch(companyCode, res.data[0]);
        }
      } catch (error) {
        console.log(error.response.data.code);
      }
    } else if (checkCode(companyCode) === "FOP") {
      try {
        const res = await axios.get(
          `https://opendatabot.com/api/v3/fop/${companyCode}?apiKey=${process.env.REACT_APP_API_KEY_OPENDATA}`,
        );

        if (res.status === 200) {
          response = transformDataFromFetch(companyCode, res.data.data);
        }
      } catch (error) {
        console.log(error);
      }
    } else alert("Введіть діючий ЄДРПОУ");

    // if (checkCode(companyCode) === "Company") {
    //   try {
    //     const res = await axios.get(`/${companyCode}.json`);
    //     response = transformDataFromFetch(companyCode, res.data[0]);
    //   } catch (error) {
    //     console.log(error.response.data.code);
    //   }
    // } else if (checkCode(companyCode) === "FOP") {
    //   try {
    //     const res = await axios.get(`/${companyCode}.json`);
    //     if (res.status === 200) {
    //       response = transformDataFromFetch(companyCode, res.data.data);
    //     }
    //   } catch (error) {
    //     console.log(error);
    //   }
    // } else alert("Введіть діючий ЄДРПОУ");

    return response;
  },
);
