import { RootState } from "../store";

export const myCompanySelector = (state: RootState) => state.myCompanySlice.myCompany;
