import React, { useEffect, useState } from "react";
import { useFormContext } from "react-hook-form";

const CounterpartyPostAdress: React.FC = () => {
  const { register, unregister } = useFormContext();

  const [isPostAdress, setIsPostAdress] = useState(false);

  useEffect(() => {
    if (!isPostAdress) {
      unregister("postAdress");
    }
  }, [isPostAdress]);

  return (
    <section className="choise-counterparty__post-adress post-adress-counterparty _section">
      <label className="post-adress-counterparty__label-check-box">
        Додати поштову адресу?
        <input
          type="checkbox"
          name="postAdress"
          className="post-adress-counterparty__check-box check-box"
          onChange={() => setIsPostAdress(!isPostAdress)}
        />
      </label>

      {isPostAdress && (
        <>
          <input
            type="text"
            placeholder="Введіть поштову адресу"
            className="post-adress-counterparty__input _input"
            {...register("postAdress", { required: true })}
          />
        </>
      )}
    </section>
  );
};

export default CounterpartyPostAdress;
