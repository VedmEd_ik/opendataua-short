import React, { useEffect } from "react";
import ListRadiobuttons from "../ListRadiobuttons";
import { fontSizes, paymentTerms } from "src/assets/data";
import { useFormContext } from "react-hook-form";
import { useSelector } from "react-redux";
import {
  documentsListSelector,
  managersSelector,
  retailersSelector,
} from "src/redux/user/selectors";

const MainInfoDocumentSection: React.FC = () => {
  const documentsList = useSelector(documentsListSelector);
  const managers = useSelector(managersSelector);
  const retailers = useSelector(retailersSelector);

  const { register, unregister, watch } = useFormContext();

  const payment = watch("documentPayment");

  useEffect(() => {
    if (payment !== paymentTerms["свій варіант"]) {
      unregister("myDocumentPayment");
    }
  }, [payment]);

  return (
    <div className="choice-document _bigSection">
      <h1 className="choice-document__label _title">Договір</h1>
      {/* Договір */}
      <section className="choice-document__document _section">
        <h2 className="_red">Оберіть необхідний договір*</h2>
        <ListRadiobuttons
          arrValues={Object.values(documentsList)}
          nameValue="documentName"
          defaultValue={documentsList["Договір_ковбаси"]}
        />
      </section>

      {/* Номер Договору */}
      <section className="choice-document__number _section">
        <h2 className="_red">Введіть номер документу*</h2>
        <input
          type="text"
          placeholder="Введіть номер договору"
          className=" _input"
          {...register("documentNumber", { required: true })}
        />
      </section>

      {/* Дата Договору */}
      <section className="choice-document__date _section">
        <h2 className="_red">Оберіть дату документу*</h2>
        <input
          type="date"
          placeholder="Введіть дату договору"
          className=" _input"
          {...register("documentDate")}
        />
      </section>

      {/* Порядок оплати */}
      <section className="choice-document__payment _section">
        <h2 className="_red">Порядок оплати товару*</h2>
        <ListRadiobuttons
          arrValues={Object.values(paymentTerms)}
          nameValue="documentPayment"
          defaultValue={paymentTerms["7"]}
        />
        {payment === paymentTerms["свій варіант"] && (
          <input
            type="text"
            className="_input"
            placeholder="Кількість календарних днів"
            {...register("myDocumentPayment", { required: true })}
          />
        )}
      </section>

      {/* Менеджер */}
      <section className="choice-document__manager _section">
        <h2>Менеджер по роботі з контрагентом</h2>
        <select
          name="manager"
          id="manager"
          title="manager"
          className="_select"
          {...register("manager")}
        >
          <option value="">Оберіть менеджера</option>
          {managers.map((manager, index) => (
            <option value={manager} key={manager + index}>
              {manager}
            </option>
          ))}
        </select>
      </section>

      {/* Торгова мережа */}
      <section className="choice-document__retailer _section">
        <h2>Торгова мережа</h2>
        <select
          name="retailer"
          id="retailer"
          title="retailer"
          className="_select"
          {...register("retailer")}
        >
          <option value="">Оберіть торгову мережу</option>
          {retailers.map((retailer, index) => (
            <option value={retailer} key={retailer + index}>
              {retailer}
            </option>
          ))}
        </select>
      </section>

      {/* Розмір шрифту */}
      <section className="choice-document__manager _section">
        <h2>Розмір шрифту</h2>
        <select
          name="fontsize"
          id="fontsize"
          title="fontsize"
          className="_select"
          {...register("fontSize", { valueAsNumber: true })}
        >
          {fontSizes.map((size, index) => (
            <option value={size} key={size + index}>
              {size}
            </option>
          ))}
        </select>
      </section>
    </div>
  );
};

export default MainInfoDocumentSection;
