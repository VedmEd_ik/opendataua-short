import React, { useEffect } from "react";
import { useFormContext } from "react-hook-form";
import { passportType, regions } from "src/assets/data";

const CounterpartyPassport: React.FC = () => {
  const [isPassport, setIsPassport] = React.useState(false);

  const { register, unregister, setValue, watch } = useFormContext();

  const passportTypeWatch = watch("passportType");

  useEffect(() => {
    if (!isPassport) {
      unregister(["passportNumber", "passportOrganName", "passportOrganRegion", "passportDate"]);
    } else {
      setValue("passportType", passportType.book);
    }
  }, [isPassport]);

  useEffect(() => {
    if (passportTypeWatch === passportType.id) {
      unregister(["passportOrganRegion"]);
    }
  }, [passportTypeWatch]);

  return (
    <section className="choise-counterparty__passport _section">
      <label>
        Додати паспортні дані?
        <input
          type="checkbox"
          value={""}
          name="addPassport"
          className="check-box"
          onChange={(e) => {
            console.log(e);
            setIsPassport(!isPassport);
          }}
        />
      </label>

      {isPassport && (
        <div className="choise-counterparty__passport-type horizontal-radiobattons">
          <label>
            <input
              type="radio"
              value={passportType.book}
              className="radio"
              {...register("passportType")}
            />
            <span className="label">Паспорт-книжка</span>
          </label>
          <label>
            <input
              type="radio"
              className="radio"
              {...register("passportType")}
              value={passportType.id}
            />
            <span className="label">ID-CARD</span>
          </label>
        </div>
      )}

      {isPassport && passportTypeWatch === passportType.book ? (
        <>
          <input
            type="text"
            className="choise-counterparty__passport-number _input"
            placeholder="Серія та номер"
            {...register("passportNumber", { required: true })}
          />
          <h2>Орган, яким видано паспорт</h2>
          <input
            {...register("passportOrganName", { required: true })}
            type="text"
            className="choise-counterparty__passport-organ _input"
            placeholder="Н-д, Дунаєвецьким РВ УМВС"
          />

          <select
            aria-label="select region organ"
            name="select-organ-regoin"
            className="choise-counterparty__select-organ _select"
            {...register("passportOrganRegion", { required: true })}
          >
            {Object.keys(regions).map((region, index) => {
              return (
                <option value={regions[region]} key={regions[region] + index}>
                  {region}
                </option>
              );
            })}
          </select>
          <h2>Дата видачі</h2>
          <input
            type="date"
            className="choise-counterparty__passport-date _input"
            placeholder="Коли виданий"
            {...register("passportDate", { required: true })}
          />
        </>
      ) : (
        ""
      )}

      {isPassport && passportTypeWatch === passportType.id ? (
        <>
          <input
            type="text"
            className="choise-counterparty__passport-number _input"
            placeholder="Номер"
            {...register("passportNumber", { required: true })}
          />
          <h2>Орган, яким видано паспорт</h2>
          <input
            type="text"
            className="choise-counterparty__passport-organ _input"
            placeholder="Н-д, 2541"
            {...register("passportOrganName", { required: true })}
          />

          <h2>Дата видачі</h2>
          <input
            type="date"
            className="choise-counterparty__passport-date _input"
            placeholder="Коли виданий"
            {...register("passportDate", { required: true })}
          />
        </>
      ) : (
        ""
      )}
    </section>
  );
};

export default CounterpartyPassport;
