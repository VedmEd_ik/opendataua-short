import React from "react";
import "./ListRadiobuttons.scss";
import { useFormContext } from "react-hook-form";

interface ListRadiobuttonsProps {
  arrValues: string[];
  nameValue: string;
  addValue?: boolean;
  placeholder?: string;
  defaultValue?: string;
}

const ListRadiobuttons: React.FC<ListRadiobuttonsProps> = ({
  arrValues,
  nameValue,
  defaultValue,
}) => {
  const { register } = useFormContext();

  return (
    <>
      {arrValues.map((value, index) => {
        return (
          <label key={index + value}>
            <input
              type="radio"
              value={value}
              className="radio"
              defaultChecked={value === defaultValue}
              {...register(nameValue)}
            />
            <span className="label">{value}</span>
          </label>
        );
      })}
    </>
  );
};

export default ListRadiobuttons;
