import React from "react";
import {
  CounterpartyBanks,
  CounterpartyBasicDoc,
  CounterpartyContacts,
  CounterpartyPassport,
  CounterpartyPostAdress,
  CounterpartySigner,
  CounterpartyTax,
} from "../";

const CounterpartySection: React.FC = () => {
  return (
    <div className="choise-counterparty _bigSection">
      <h1 className="choise-counterparty__title _title">Інформація про контрагента</h1>
      <CounterpartySigner />
      <CounterpartyBasicDoc />
      <CounterpartyContacts />
      <CounterpartyBanks />
      <CounterpartyPostAdress />
      <CounterpartyPassport />
      <CounterpartyTax />
    </div>
  );
};

export default CounterpartySection;
