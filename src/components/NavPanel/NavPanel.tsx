import React from "react";
import { useReactToPrint } from "react-to-print";
import "./NavPanel.scss";

type NavPanelProps = {
  refComponentToPrint: React.MutableRefObject<HTMLElement>;
  documenTitleToPrint: string;
};

const NavPanel: React.FC<NavPanelProps> = ({ refComponentToPrint, documenTitleToPrint }) => {
  const onPrint = useReactToPrint({
    content: () => refComponentToPrint.current,
    documentTitle: documenTitleToPrint,
  });

  return (
    <section className="output-data__nav">
      <div className="output-data__nav-text _subtitle">Згенеровано наступний документ</div>
      <div className="output-data__nav-actions actions">
        <button type="button" className="actions__print _icon-print-2" onClick={onPrint}>
          Друк
        </button>
      </div>
    </section>
  );
};

export default NavPanel;
