import React from "react";
import style from "./Error.module.scss";

type ErrorProps = {
  text: string;
};

const Error: React.FC<ErrorProps> = () => {
  return (
    <div className={style.wrapper}>
      <section className={style.error}>
        <h2 className={style.error__title}>
          Ой! <span className="_icon-face-frown"></span>
        </h2>
        <h3 className={style.error__subtitle}>Помилка сервера!</h3>
        <div className={style.error__text}>
          Нам дуже прикро, що сталася така прикра ситуація. <br /> Перевірте, будь ласка, чи
          правильний код ви ввели. <br />
          Якщо все правильно, то швидше за все такої компанії чи ФОП не існує.
        </div>
      </section>
    </div>
  );
};

export default Error;
