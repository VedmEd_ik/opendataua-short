import React from "react";
import ListRadiobuttons from "../ListRadiobuttons";

import { useSelector } from "react-redux";
import { firmDataSelector } from "src/redux/user/selectors";

const MyCompanySection: React.FC = () => {
  const firmData = useSelector(firmDataSelector);

  return (
    <div className="choice-company _bigSection">
      <h1 className="choice-company__title _title ">Ваша компанія чи ФОП</h1>

      <section className="_section">
        <h2 className="_red">Оберіть свою компанію чи ФОП*</h2>
        <ListRadiobuttons
          arrValues={Object.keys(firmData)}
          nameValue="myCompanyname"
          defaultValue={Object.keys(firmData)[2]}
        />
      </section>
    </div>
  );
};

export default MyCompanySection;
