import React, { useEffect, useState } from "react";
import style from "./Input.module.scss";

interface InputProps {
  placeHolder: string;
  actionFunc: (value: string) => void;
  classN?: string;
  inputType: string;
}

const Input: React.FC<InputProps> = ({ inputType, classN = "", placeHolder, actionFunc }) => {
  const [inputValue, setInputValue] = useState("");

  const handleChange = (value: string) => {
    setInputValue(value);
  };

  useEffect(() => {
    actionFunc(inputValue);
  }, [inputValue]);
  return (
    <div className="input-wrap">
      <input
        type={inputType}
        placeholder={placeHolder}
        onChange={(event) => handleChange(event.target.value)}
        value={inputValue}
        className={style._input + classN}
      />
    </div>
  );
};

export default Input;
