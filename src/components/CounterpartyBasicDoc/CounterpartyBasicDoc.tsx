import React, { useEffect } from "react";
import { basisDocument } from "src/assets/data";
import ListRadiobuttons from "../ListRadiobuttons";
import { useFormContext } from "react-hook-form";

const CounterpartyBasicDoc: React.FC = () => {
  const { register, unregister, watch } = useFormContext();
  const basisDocumentWatch = watch("basisDocument");

  useEffect(() => {
    if (basisDocumentWatch !== basisDocument["Інший"]) {
      unregister(["otherBasisDocumentName", "otherBasisDocumentNumber", "otherBasisDocumentDate"]);
    }
  }, [basisDocumentWatch]);

  return (
    <section className="choise-counterparty__basis _section">
      <h2 className="choise-counterparty__basis-label _red">
        Документ, на підставі якого діє підписант*
      </h2>

      <ListRadiobuttons
        nameValue="basisDocument"
        arrValues={Object.values(basisDocument)}
        defaultValue={basisDocument["Основний"]}
      />

      {basisDocumentWatch === basisDocument["Інший"] && (
        <>
          <input
            type="text"
            placeholder="Введіть назву документу"
            className="choise-counterparty__basis-authority-number _input"
            {...register("otherBasisDocumentName")}
          />
          <input
            type="text"
            placeholder="Введіть номер документу"
            className="choise-counterparty__basis-authority-number _input"
            {...register("otherBasisDocumentNumber")}
          />
          <input
            type="date"
            placeholder="Введіть дату документу"
            className="choise-counterparty__basis-authority-date _input"
            {...register("otherBasisDocumentDate")}
          />
        </>
      )}
    </section>
  );
};

export default CounterpartyBasicDoc;
