import React, { useEffect, useState } from "react";
import { useFormContext } from "react-hook-form";

const CounterpartyBanks: React.FC = () => {
  const [isAddBank, setIsAddBank] = useState(false);

  useEffect(() => {
    if (!isAddBank) {
      unregister(["accBank2", "nameBank2"]);
    }
  }, [isAddBank]);

  const { register, unregister } = useFormContext();

  return (
    <section className="choise-counterparty__banks _section">
      <h2 className="choise-counterparty__banks-bank">Введіть банківські реквізити контрагента</h2>
      <input
        type="text"
        className="choise-counterparty__banks-bank-account _input"
        placeholder="IBAN: UA..."
        {...register("accBank1")}
      />
      <input type="text" placeholder="Назва банку" className="_input" {...register("nameBank1")} />
      <label>
        Додати ще один банк?
        <input
          type="checkbox"
          name="addBank"
          className="check-box"
          onChange={() => setIsAddBank(!isAddBank)}
        />
      </label>

      {isAddBank && (
        <>
          <input
            type="text"
            className="choise-counterparty__banks-bank-account _input"
            placeholder="IBAN: UA..."
            {...register("accBank2")}
          />
          <input
            type="text"
            className="choise-counterparty__banks-bank-name _input"
            placeholder="Назва банку"
            {...register("nameBank2")}
          />
        </>
      )}
    </section>
  );
};

export default CounterpartyBanks;
