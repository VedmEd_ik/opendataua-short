import { useEffect } from "react";
import { useFormContext } from "react-hook-form";

const InputsForm: React.FC = () => {
  const { register, watch } = useFormContext();

  const numberDocumentWatch = watch("numberDocument");
  const dataDocumentWatch = watch("dataDocument");

  useEffect(() => {
    console.log(numberDocumentWatch);
    console.log(dataDocumentWatch);
  }, [numberDocumentWatch, dataDocumentWatch]);

  return (
    <>
      {/* Номер Договору */}
      <section className="choice-document__number _section">
        <h2 className="_red">Введіть номер документу*</h2>
        <input
          type="text"
          placeholder="Введіть номер договору"
          className=" _input"
          {...register("numberDocument", { required: true })}
        />
      </section>

      {/* Дата Договору */}
      <section className="choice-document__date _section">
        <h2 className="_red">Оберіть дату документу*</h2>
        <input
          type="date"
          placeholder="Введіть дату договору"
          className=" _input"
          {...register("dataDocument")}
        />
      </section>
    </>
  );
};

export default InputsForm;
