import React, { useEffect } from "react";
import ListRadiobuttons from "../ListRadiobuttons";

import { useFormContext } from "react-hook-form";
import { signersList } from "src/assets/data";

const CounterpartySigner: React.FC = () => {
  const { register, unregister, watch } = useFormContext();
  const signerWatch = watch("counterpartySigner");

  useEffect(() => {
    if (signerWatch !== signersList[1]) {
      unregister(["signerOtherName", "signerOtherRole"]);
    }
  }, [signerWatch]);

  return (
    <section className="choise-counterparty__signer _section">
      <h2 className="choise-counterparty__signer-label _red">Підписант*</h2>

      <ListRadiobuttons
        arrValues={signersList}
        nameValue="counterpartySigner"
        defaultValue={signersList[0]}
      />

      {signerWatch === signersList[1] && (
        <>
          <input
            type="text"
            placeholder="Введіть повне ПІБ підписанта"
            className="_input"
            {...register("signerOtherName", { required: true })}
          />
          <input
            type="text"
            placeholder="Введіть посаду підписанта"
            className="_input"
            {...register("signerOtherRole", { required: true })}
          />
        </>
      )}
    </section>
  );
};

export default CounterpartySigner;
