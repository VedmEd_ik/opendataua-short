import React, { useEffect } from "react";
import ListRadiobuttons from "../ListRadiobuttons";
import { singleGroups, systemTax } from "src/assets/data";
import { useFormContext } from "react-hook-form";
import { counterPartySelector } from "src/redux/counterparty/selectors";
import { useSelector } from "react-redux";

const CounterpartyTax: React.FC = () => {
  const { register, watch, setValue } = useFormContext();
  const isPDVWatch = watch("isPDV");
  const taxSystesmWatch = watch("systemsTax");
  const isTaxmWatch = watch("isTax");

  const counterParty = useSelector(counterPartySelector);
  const isPDV = counterParty.pdvStatus === "active";
  const ipn = counterParty.pdvCode;

  useEffect(() => {
    if (isTaxmWatch) {
      setValue("isPDV", isPDV);
      setValue("IPN", ipn);
    }
  }, [isTaxmWatch]);
  useEffect(() => {
    if (!isPDVWatch) {
      setValue("IPN", "");
    } else {
      setValue("IPN", ipn);
    }
  }, [isPDVWatch]);

  return (
    <section className="choise-counterparty__tax _section">
      <label>
        Додати дані про систему оподаткування?
        <input type="checkbox" name="isTax" className="check-box" {...register("isTax")} />
      </label>

      {isTaxmWatch && (
        <>
          <div className="choise-counterparty__tax-type horizontal-radiobattons">
            <ListRadiobuttons
              arrValues={Object.keys(systemTax)}
              nameValue="systemsTax"
              defaultValue={systemTax["Спрощена"]}
            />
          </div>

          {taxSystesmWatch === systemTax["Спрощена"] && (
            <>
              <div className="choise-counterparty__single-groups-title">
                Оберіть групу єдиного податку
              </div>
              <div className="choise-counterparty__single-groups horizontal-radiobattons">
                <ListRadiobuttons
                  arrValues={Object.keys(singleGroups)}
                  nameValue="singleGroup"
                  defaultValue={"2"}
                />
              </div>
            </>
          )}

          <label>
            Платник ПДВ?
            <input type="checkbox" className="check-box" {...register("isPDV")} />
          </label>

          {isPDVWatch && (
            <label>
              ІПН: <input type="text" className="_input" {...register("IPN")} />
            </label>
          )}
        </>
      )}
    </section>
  );
};

export default CounterpartyTax;
