import "./LogIn.scss";
import useAuth from "../../hooks/use-auth";
import { removeUser } from "../../redux/user/slice";
import { removeCounterparty } from "../../redux/counterparty/slice";
import { useAppDispatch } from "src/redux/store";

const LogIn: React.FC = () => {
  const { isAuth, email } = useAuth();

  const dispatch = useAppDispatch();

  const handelLogout = () => {
    dispatch(removeUser());
    dispatch(removeCounterparty());
  };

  return (
    <div className="login">
      {isAuth ? (
        <>
          <div className="login__name">
            Вхід виконано: <span>{email}</span>
          </div>
          <div className="login__logout" onClick={handelLogout}>
            Вийти
          </div>
        </>
      ) : (
        <div>Вхід не виконано</div>
      )}
    </div>
  );
};

export default LogIn;
