import React from "react";
import style from "./QrCode.module.scss";

type QrCodeProps = {
  document: string;
  nameDocument: string;
  number: string;
  date: string;
  myCompany: string;
  counterparty: string;
};

const QrCode: React.FC<QrCodeProps> = ({
  document,
  nameDocument,
  number,
  date,
  myCompany,
  counterparty,
}) => {
  const text = `${document} ${nameDocument} № ${number} від ${date} року, укладений між ${myCompany} та ${counterparty}. Відповідальний за роботу з контрагентом: Горний Ю.Ю.`;

  return (
    <div className={style["qr-code"]}>
      <img src={`http://api.qrserver.com/v1/create-qr-code/?data=${text}`} alt="" />
    </div>
  );
};

export default QrCode;
