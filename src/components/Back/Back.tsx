import React from "react";
import { useNavigate } from "react-router-dom";
import "./Back.scss";
import { removeCounterparty } from "src/redux/counterparty/slice";
import { removeDocumentData } from "src/redux/document/slice";
import { removeMyCompany } from "src/redux/myCompany/slice";
import { useAppDispatch } from "src/redux/store";

const Back: React.FC = () => {
  const navigate = useNavigate();
  const dispatch = useAppDispatch();

  const handleClickBackButton = () => {
    dispatch(removeCounterparty());
    dispatch(removeDocumentData());
    dispatch(removeMyCompany());
    navigate("/check-company");
  };

  return (
    <section className="back">
      <button
        type="button"
        onClick={handleClickBackButton}
        className="back__arrow _icon-back-arrow"
      >
        Назад
      </button>
    </section>
  );
};

export default Back;
