import React, { useEffect, useState } from "react";
import { useFormContext } from "react-hook-form";

type ContactsType = {
  addedPhone: boolean;
  addedEmail: boolean;
};

const CounterpartyContacts: React.FC = () => {
  const [contactsState, setContactsState] = useState<ContactsType>({
    addedPhone: false,
    addedEmail: false,
  });

  const { register, unregister } = useFormContext();

  useEffect(() => {
    !contactsState.addedEmail && unregister("addEmail");
  }, [contactsState.addedEmail]);
  useEffect(() => {
    !contactsState.addedPhone && unregister("addPhone");
  }, [contactsState.addedPhone]);

  return (
    <section className="choise-counterparty__contacts _section">
      <h2 className="choise-counterparty__contacts-label ">Контактні дані</h2>

      <div className="choise-counterparty__contacts-container">
        <input
          type="tel"
          name="phone"
          className="choise-counterparty__contacts-tel _input"
          placeholder="Введіть номер телефону"
          {...register("phone")}
        />

        <label
          className={!contactsState.addedPhone ? "_icon-plus_circle" : "_icon-minus-on-circle"}
        >
          {""}
          <input
            type="checkbox"
            onChange={() =>
              setContactsState({ ...contactsState, addedPhone: !contactsState.addedPhone })
            }
          />
        </label>
      </div>

      {contactsState.addedPhone && (
        <input
          type="tel"
          name="phone"
          className="choise-counterparty__contacts-tel _input"
          placeholder="Введіть номер телефону"
          {...register("addPhone")}
        />
      )}

      <div className="choise-counterparty__contacts-container">
        <input
          type="email"
          name="email"
          className="choise-counterparty__contacts-email _input"
          placeholder="Введіть електронну адресу"
          {...register("email")}
        />

        <label
          className={!contactsState.addedEmail ? "_icon-plus_circle" : "_icon-minus-on-circle"}
        >
          {""}
          <input
            type="checkbox"
            onChange={() =>
              setContactsState({ ...contactsState, addedEmail: !contactsState.addedEmail })
            }
          />
        </label>
      </div>
      {contactsState.addedEmail && (
        <input
          type="email"
          name="email"
          className="choise-counterparty__contacts-email _input"
          placeholder="Введіть електронну адресу"
          {...register("addEmail")}
        />
      )}
    </section>
  );
};

export default CounterpartyContacts;
