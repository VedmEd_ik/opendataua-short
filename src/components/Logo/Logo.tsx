import React from "react";
import "./Logo.scss";
import logo from "../../assets/logo/Logo.svg";
import { Link } from "react-router-dom";

const Logo: React.FC = () => {
  return (
    <div className="logo-container">
      <Link to="/" className="logo">
        <img alt="Logo" src={logo}></img>
      </Link>
    </div>
  );
};

export default Logo;
