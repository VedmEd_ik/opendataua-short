import React, { useEffect } from "react";
import { Outlet, useLocation } from "react-router-dom";
import { Footer, Header } from "../components";
import { useAppDispatch } from "src/redux/store";
import { removeCounterparty } from "src/redux/counterparty/slice";

const MainLayout: React.FC = () => {
  const { pathname } = useLocation();
  const dispatch = useAppDispatch();

  useEffect(() => {
    if (pathname != "/contract") {
      dispatch(removeCounterparty());
    }
  }, [pathname]);

  return (
    <div className="wrapper">
      <Header />
      <div className="content">
        <Outlet />
      </div>
      <Footer />
    </div>
  );
};

export default MainLayout;
