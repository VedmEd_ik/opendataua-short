import React from "react";
import { SubmitHandler } from "react-hook-form";
import { useForm, FormProvider } from "react-hook-form";
import { useSelector } from "react-redux";
import { counterPartySelector } from "src/redux/counterparty/selectors";
import { setDocumentData, setStatusDocument } from "src/redux/document/slice";
import { setMyCompany } from "src/redux/myCompany/slice";
import { useAppDispatch } from "src/redux/store";
import { firmDataSelector } from "src/redux/user/selectors";
import { todayDate, transformDataFromForm } from "src/utils/functions";

interface IFormContext {
  children: React.ReactNode;
}

export type FormValues = {
  documentName?: string;
  documentNumber?: string;
  documentDate?: string;
  manager?: string;
  retailer?: string;
  fontSize?: number;
  basisDocument?: string;
  counterpartySigner?: string;
  documentPayment?: string;
  myDocumentPayment?: string;
  myCompanyname?: string;
  signerOtherName?: string;
  signerOtherRole?: string;
  phone?: string;
  addPhone?: string;
  email?: string;
  addEmail?: string;
  postAdress?: string;
  accBank1?: string;
  nameBank1?: string;
  accBank2?: string;
  nameBank2?: string;
  otherBasisDocumentDate?: string;
  otherBasisDocumentName?: string;
  otherBasisDocumentNumber?: string;
  passportType?: string;
  passportDate?: string;
  passportNumber?: string;
  passportOrganName?: string;
  passportOrganRegion?: string;
  isTax?: boolean;
  systemsTax?: string;
  singleGroup?: string;
  isPDV?: boolean;
  IPN?: string;
};

const FormContext: React.FC<IFormContext> = ({ children }) => {
  const dispatch = useAppDispatch();
  const myFirmsData = useSelector(firmDataSelector);
  const counterParty = useSelector(counterPartySelector);
  const phone = counterParty.phones ? counterParty.phones.join(", ") : "";

  const defaultValuesForm: FormValues = {
    documentDate: todayDate(false).toString(),
    fontSize: 9,
    email: counterParty.email,
    phone: phone,
    isTax: false,
  };

  const methods = useForm<FormValues>({
    defaultValues: defaultValuesForm,
    mode: "onChange",
  });

  const onSubmit: SubmitHandler<FormValues> = (data) => {
    dispatch(setDocumentData(transformDataFromForm(data, counterParty)));
    dispatch(setStatusDocument());
    dispatch(setMyCompany(myFirmsData[data.myCompanyname]));
  };

  return (
    <FormProvider {...methods}>
      <form onSubmit={methods.handleSubmit(onSubmit)} className="input-data__form">
        {children}
        <input
          type="submit"
          className="generate-document-button _button _icon-document"
          value={"Згенерувати документ"}
        />
      </form>
    </FormProvider>
  );
};

export default FormContext;
