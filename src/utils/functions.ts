import {
  AxiosResponseCompanyNew,
  AxiosResponseFOP,
  DataFromAxios,
  FactorType,
  FactorTypeNew,
  NewCounterPartyType,
} from "src/redux/counterparty/types";
import qrcode from "qrcode";
import { FormValues } from "src/hoc/FormContext";
import { basisDocument, signersList, systemTax } from "src/assets/data";
import { dataFromFormType } from "src/redux/document/types";
import { MyCompanyType } from "src/@types/types";

// Функція, яка форматує дату типу "2022-03-21" на "21.03.2022"
export const formatDate = (date: string) => {
  const newDate = new Date(date);
  const day = newDate.getDate() < 10 ? "0" + newDate.getDate() : newDate.getDate();
  const month =
    newDate.getMonth() + 1 < 10 ? "0" + (newDate.getMonth() + 1) : newDate.getMonth() + 1;
  const year = newDate.getFullYear();
  return day + "." + month + "." + year;
};

// Функція, яка відділяє перше слово "Договір" від решти назви документу
export const parseNameDocument = (fullNameDocument: string) => {
  const arr = fullNameDocument.split(" ");
  const document = arr[0];
  const nameDocument = arr.slice(1).reduce((acc, word) => acc + " " + word, "");
  return { document, nameDocument };
};

// Функція, яка перевіряє код ЄДРПОУ - чи ЮО чи ФОП
export const checkCode = (code: string) => {
  const regFOP = /\d{10}/;
  const regCompany = /\d{8}/;
  if (code.length === 8 && code.match(regCompany)) {
    return "Company";
  } else if (code.length === 10 && code.match(regFOP)) {
    return "FOP";
  }
};

// Функція, яка повертає сьогоднішню дату у формат: "21.03.2022"
export const todayDate = (isFormat = true) => {
  const date = new Date();
  const day = date.getDate();
  const month = date.getMonth() + 1;
  const yy = date.getFullYear();

  let dd = String(day);
  let mm = String(month);

  if (day < 10) dd = "0" + day;

  if (month < 10) mm = "0" + month;

  return isFormat ? `${dd}.${mm}.${yy}` : `${yy}-${mm}-${dd}`;
};

// Функція, яка відслідковує натискання клавіші Enter
export const handleKeyDown = (event: React.KeyboardEvent, handleFunc: () => void) => {
  if (event.key.toLowerCase() === "enter") {
    handleFunc();
  }
};

// Функція, яка робить першу літеру прописною
export const capitalize = (str: string) => str.charAt(0).toUpperCase() + str.slice(1);

// Функція, яка витягає необхідний ризик-фактор
export const getFactor = (factorGroup: string, type: string, data: DataFromAxios): FactorType[] => {
  if (data.factors) {
    const inf = data.factors.filter(
      (current) => current.factorGroup === factorGroup && current.type === type,
    );
    return inf;
  }
};

// Функція, яка генерує qr-код
export const genQrCode = async (
  document: string,
  number: string,
  date: string,
  myCompanyName: string,
  counterpartyName: string,
  manager?: string,
  retailer?: string,
) => {
  const valueString = `${document} № ${number} від ${formatDate(
    date,
  )}, укладений між ${myCompanyName} та ${counterpartyName}.${
    manager && ` Менеджер по роботі з клієнтом: ${manager}`
  }.${retailer && ` Торгова мережа: ${retailer}`}`;
  const qrCodeImage = await qrcode.toDataURL(valueString);
  return qrCodeImage;
};

// Функція, яка записує дані в session storage
export const setDataToSessionStorage = (
  key: "counterparty" | "myCompany" | "mainInfoDocument",
  data: NewCounterPartyType | MyCompanyType | dataFromFormType,
) => {
  const getDataSessionStorage = sessionStorage.getItem(key);

  if (getDataSessionStorage) {
    const newData = JSON.stringify({ ...JSON.parse(getDataSessionStorage), ...data });
    sessionStorage.setItem(key, newData);
  } else {
    sessionStorage.setItem(key, JSON.stringify(data));
  }
};

// Функція, яка повертає правильне написання порядку оплати в договорі
export const paymentProcedure = (payment: string, contractObject: "товар" | "послуги") => {
  if (contractObject === "товар") {
    switch (payment) {
      case "шляхом 100% передоплати":
        return payment;
      case "по факту":
        return `${payment} поставки Товару`;
      default:
        return `${payment} з моменту поставки Товару`;
    }
  } else if (contractObject === "послуги") {
    switch (payment) {
      case "шляхом 100% передоплати":
        return payment;
      case "по факту":
        return `${payment} надання Послуг`;
      default:
        return `${payment} з моменту надання Послуг`;
    }
  }
};

// Функція, яка витягає необхіднe значення з factor
export const getFactorValue = (
  factorGroup: string,
  type: string,
  key: keyof FactorTypeNew,
  data: AxiosResponseFOP,
): string | null => {
  const current = data.factors.find(
    (current) => current.factorGroup === factorGroup && current.type === type,
  );

  if (!current) return;

  const isCurrentKey = Object.keys(current).includes(key);

  if (isCurrentKey) {
    const isString = typeof current[key] === "string";
    const isNumber = typeof current[key] === "number";
    const isBoolean = typeof current[key] === "boolean";

    if (isString || isNumber || isBoolean) {
      return String(current[key]);
    }
  }

  return null;
};

function tax(data: FormValues) {
  if (data.isTax) {
    return data.systemsTax === systemTax["Загальна"]
      ? "Платник податку на загальній системі"
      : `Платник єдиного податку ${data.singleGroup} групи`;
  } else {
    return null;
  }
}

function pdv(data: FormValues) {
  if (data.isTax) {
    return data.isPDV ? "із реєстрацією ПДВ" : "без реєстрації ПДВ";
  } else return null;
}

function ipn(data: FormValues) {
  return data.isTax ? data.IPN : null;
}

function signerBasisDoc(data: FormValues, isFOP: boolean) {
  if (data.basisDocument === basisDocument["Інший"]) {
    const nameDoc = data.otherBasisDocumentName;
    const numberDoc = data.otherBasisDocumentNumber;
    const dateDoc = data.otherBasisDocumentDate ? formatDate(data.otherBasisDocumentDate) : "";

    if (nameDoc && numberDoc && dateDoc) {
      return `${nameDoc} № ${numberDoc} від ${dateDoc} року`;
    } else if (nameDoc && numberDoc && !dateDoc) {
      return `${nameDoc} № ${numberDoc}`;
    } else if (nameDoc && !numberDoc && dateDoc) {
      return `${nameDoc} від ${dateDoc} року`;
    } else if (nameDoc && !numberDoc && !dateDoc) {
      return `${nameDoc}`;
    } else {
      return `______________________________`;
    }
  } else {
    return isFOP ? "Виписка з ЄДР" : "Статут";
  }
}

function signer(data: FormValues, dataFromStore: NewCounterPartyType) {
  if (data.counterpartySigner === signersList[1]) {
    return {
      name: data.signerOtherName,
      role: data.signerOtherRole,
    };
  } else {
    return {
      name: dataFromStore.ceo,
      role: dataFromStore.isFOP ? "Керівник" : "Директор",
    };
  }
}

// Функція, яка перетворює дані, отримані від сервера у формат, необхідний для подальшої роботи програми
export const transformDataFromFetch = (
  counterpartyCode: string,
  dataAxios: AxiosResponseFOP | AxiosResponseCompanyNew,
): NewCounterPartyType => {
  const isFOP = counterpartyCode.length === 10;

  return {
    isFOP: isFOP,
    status: isFOP
      ? (dataAxios as AxiosResponseFOP).registry.status
      : (dataAxios as AxiosResponseCompanyNew).status,
    // =========== Основна обов'язкова інформація для договору ===========
    fullName: isFOP
      ? `Фізична особа-підприємець ${(dataAxios as AxiosResponseFOP).registry.fullName}`
      : (dataAxios as AxiosResponseCompanyNew).full_name,
    shortName: isFOP
      ? `ФОП ${(dataAxios as AxiosResponseFOP).registry.fullName}`
      : (dataAxios as AxiosResponseCompanyNew).short_name,
    code: isFOP
      ? (dataAxios as AxiosResponseFOP).registry.code.toString()
      : (dataAxios as AxiosResponseCompanyNew).code,
    ceo: isFOP
      ? (dataAxios as AxiosResponseFOP).registry.fullName
      : (dataAxios as AxiosResponseCompanyNew).ceo_name,
    address: isFOP
      ? (dataAxios as AxiosResponseFOP).registry.address.address
      : (dataAxios as AxiosResponseCompanyNew).location,
    phones: isFOP ? (dataAxios as AxiosResponseFOP).registry?.phones : null,
    email: isFOP ? (dataAxios as AxiosResponseFOP).registry?.email : null,

    // =========== Додаткова не обов'зякова інформація для договору ===========
    pdvStatus: isFOP
      ? getFactorValue("tax", "vat", "status", dataAxios as AxiosResponseFOP)
      : (dataAxios as AxiosResponseCompanyNew).pdv_status,
    pdvCode: isFOP
      ? getFactorValue("tax", "vat", "number", dataAxios as AxiosResponseFOP)
      : (dataAxios as AxiosResponseCompanyNew).pdv_code,
    pdvText: isFOP && getFactorValue("tax", "vat", "text", dataAxios as AxiosResponseFOP),
    singleTaxStatus: isFOP
      ? getFactorValue("tax", "singletax", "status", dataAxios as AxiosResponseFOP)
      : null,
    singleTaxIcon: isFOP
      ? getFactorValue("tax", "singletax", "icon", dataAxios as AxiosResponseFOP)
      : null,
    singleTaxText: isFOP
      ? getFactorValue("tax", "singletax", "text", dataAxios as AxiosResponseFOP)
      : null,
    singleTaxGroup: isFOP
      ? getFactorValue("tax", "singletax", "group", dataAxios as AxiosResponseFOP)
      : null,
    pdvStatusIcon: isFOP && getFactorValue("tax", "vat", "icon", dataAxios as AxiosResponseFOP),

    // =========== Додаткова інформація для відображення на сторінці CheckCompany ===========
    primaryActivity: isFOP ? (dataAxios as AxiosResponseFOP).registry.primaryActivity : null,
    activities: isFOP ? (dataAxios as AxiosResponseFOP).registry.activities : null,

    bankruptcy: isFOP ? (dataAxios as AxiosResponseFOP).registry?.bankruptcy?.stateText : null,

    beneficiaries: !isFOP && (dataAxios as AxiosResponseCompanyNew).beneficiaries,

    taxDebtText: isFOP && getFactorValue("tax", "debt", "text", dataAxios as AxiosResponseFOP),
    taxDebtIcon: isFOP && getFactorValue("tax", "debt", "icon", dataAxios as AxiosResponseFOP),

    warTerritoryStatus:
      isFOP &&
      getFactorValue("edr", "warTerritory", "activeFightStatus", dataAxios as AxiosResponseFOP),
    warTerritoryText:
      isFOP && getFactorValue("edr", "warTerritory", "text", dataAxios as AxiosResponseFOP),
    warTerritoryIcon:
      isFOP && getFactorValue("edr", "warTerritory", "icon", dataAxios as AxiosResponseFOP),
  };
};

export const transformDataFromForm = (
  dataFromForm: FormValues,
  dataFromStore: NewCounterPartyType,
): dataFromFormType => {
  return {
    documentName: dataFromForm.documentName,
    documentNumber: dataFromForm.documentNumber,
    documentDate: dataFromForm.documentDate,
    documentPayment: dataFromForm.myDocumentPayment
      ? `протягом ${dataFromForm.myDocumentPayment} календарних днів`
      : dataFromForm.documentPayment,
    documentFontSize: dataFromForm.fontSize,
    manager: dataFromForm.manager,
    retailer: dataFromForm.retailer,

    signer: signer(dataFromForm, dataFromStore),
    signerBasisDocument: signerBasisDoc(dataFromForm, dataFromStore.isFOP),
    postAddress: dataFromForm.postAdress,
    passport: {
      status:
        dataFromForm.passportDate &&
        dataFromForm.passportNumber &&
        dataFromForm.passportOrganName &&
        true,
      number: dataFromForm.passportNumber,
      date: dataFromForm.passportDate,
      organName: dataFromForm.passportOrganName,
      organRegion: dataFromForm.passportOrganRegion,
      passportType: dataFromForm.passportType,
    },
    phones: dataFromForm.phone,
    email: dataFromForm.email,
    phonesAdd: dataFromForm.addPhone,
    emailAdd: dataFromForm.addEmail,
    bankDetails: {
      bank1: {
        "bank-account": dataFromForm.accBank1,
        bank: dataFromForm.nameBank1,
      },
      bank2: {
        "bank-account": dataFromForm.accBank2,
        bank: dataFromForm.nameBank2,
      },
    },
    isTax: dataFromForm.isTax,
    taxationSystem: tax(dataFromForm),
    PDV: pdv(dataFromForm),
    IPN: ipn(dataFromForm),
  };
};
