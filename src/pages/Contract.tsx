import React, { useEffect, memo } from "react";
import { useSelector } from "react-redux";
import { statusDocumentSelector } from "src/redux/document/selectors";
import { CounterpartySection, MainInfoDocumentSection } from "../components";
import OutDocumentVerest from "../companies/Verest/OutDocument";
import useAuth from "src/hooks/use-auth";
import MyCompanySection from "src/components/MyCompanySection";

import FormContext from "src/hoc/FormContext";

import "./Contract.scss";

const Contract: React.FC = memo(() => {
  const { email } = useAuth();
  const dataStatus = useSelector(statusDocumentSelector);

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  const outDocument = (email: string) => {
    switch (email) {
      case "verest.juryst@gmail.com":
        return <OutDocumentVerest />;
      case "edik171989@gmail.com":
        return <></>;
    }
  };

  console.log("Рендер компоненту Contract");

  return (
    <>
      <section className="input-data">
        <FormContext>
          <MainInfoDocumentSection />

          <MyCompanySection />

          <CounterpartySection />
        </FormContext>
      </section>

      <section className="output-data">
        {dataStatus ? outDocument(email) : <div>Тут буде згенеровано ваш документ</div>}
      </section>
    </>
  );
});

export default Contract;
