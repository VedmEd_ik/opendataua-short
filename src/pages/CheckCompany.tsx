import React, { useState, useEffect, useRef } from "react";
import { useSelector } from "react-redux";
import { ProgressBar } from "react-loader-spinner";
import { Link } from "react-router-dom";
import NavPanel from "../components/NavPanel/NavPanel";
import Error from "../components/Error/Error";
import { fetchCompany } from "src/redux/counterparty/fetchCompanyAsyncThunks";
import { removeCounterparty } from "src/redux/counterparty/slice";
import { NewCounterPartyType } from "src/redux/counterparty/types";
import { useAppDispatch } from "src/redux/store";
import { SubmitHandler, useForm } from "react-hook-form";
import { removeDocumentData } from "src/redux/document/slice";
import { removeMyCompany } from "src/redux/myCompany/slice";
import { counterPartySelector, statusCounterPartySelector } from "src/redux/counterparty/selectors";
import "./CheckCompany.scss";

type FullInfoSatate = {
  [key: string]: { key: string; data: JSX.Element | JSX.Element[] | string };
};

export type FormValues = {
  code: string;
};

const CheckCompany: React.FC = () => {
  const { register, handleSubmit } = useForm<FormValues>();
  const [fullInfo, setFullInfo] = useState<FullInfoSatate>({});
  const dispatch = useAppDispatch();
  const data = useSelector(counterPartySelector);
  const statusCheckedCompany = useSelector(statusCounterPartySelector);
  const companyInfo = useRef<HTMLElement>();

  const formatDataCounterpartyAndSetState = (data: NewCounterPartyType) => {
    setFullInfo({
      fullName: {
        key: "Повне найменування",
        data: data.fullName.toLocaleUpperCase(),
      },
      shortName: {
        key: "Скорочене найменування",
        data: data.shortName,
      },
      code: { key: "Код ЄДРПОУ", data: data.code },
      status: {
        key: "Статус",
        data:
          data.status === "зареєстровано" ? (
            <span>
              {data.status} <span className="_icon-check-box_checked-2 _green" />
            </span>
          ) : (
            <span>
              {data.status} <span className="_icon-cross _red" />
            </span>
          ),
      },
      location: { key: "Місцезнаходження", data: data.address },
      ceoName: {
        key: "Директор",
        data: data.ceo ? data.ceo : null,
      },
      phones: {
        key: "Телефони",
        data:
          data.phones && data.phones.length > 0
            ? data.phones.map((el, index) => <span key={el + index.toString()}> {el}; </span>)
            : null,
      },
      email: {
        key: "Електронна адреса",
        data: data.email ? data.email : null,
      },
      primaryActivity: { key: "Основний КВЕД", data: data.primaryActivity },
      activities: {
        key: "КВЕДи",
        data: data.activities
          ? data.activities.map((current, index) => (
              <div key={current + index.toString()}>
                {current.code}. {current.name}
                <br />
              </div>
            ))
          : null,
      },
      beneficiaries: {
        key: "Бенефеціари",
        data: data.beneficiaries
          ? data.beneficiaries.map((current, index) => {
              return (
                <div key={current + index.toString()}>
                  <div>{current.title}</div>
                  {current.role && <div>{current.role}</div>}
                  {current.capital && (
                    <div>Частка в статутному капіталі: {current.capital} грн.</div>
                  )}
                  <div>Адреса: {current.location}</div>
                  <br />
                </div>
              );
            })
          : null,
      },
      PDV: {
        key: "ПДВ",
        data:
          data.pdvStatus === "active" ? (
            <>
              <span>Платник ПДВ </span>
              <span>{data.pdvStatusIcon}</span>
            </>
          ) : data.pdvStatus === "nonactive" ? (
            <>
              <span>{data.pdvText}</span>
              <span>{data.pdvStatusIcon}</span>
            </>
          ) : null,
      },
      IPN: { key: "ІПН", data: +data.pdvCode ? data.pdvCode : null },
      singleTax: {
        key: "Єдиний податок",
        data:
          data.singleTaxStatus && data.singleTaxStatus === "active" ? (
            <>
              <span>{data.singleTaxText}</span>
              <span>{data.singleTaxIcon}</span>
            </>
          ) : null,
      },
      taxDebt: {
        key: "Податковий борг",
        data: data.taxDebtText ? (
          <>
            <span>{data.taxDebtText}</span>
            <span>{data.taxDebtIcon}</span>
          </>
        ) : null,
      },
      warTerritory: {
        key: "Територія, де ведуться бойові дії",
        data:
          data.warTerritoryStatus === "true" ? (
            <>
              <span>{data.warTerritoryText}</span>
              <span>{data.warTerritoryIcon}</span>
            </>
          ) : null,
      },
      bankruptcy: { key: "Банкрутство", data: data.bankruptcy && <span>{data.bankruptcy}</span> },
    });
  };

  useEffect(() => {
    if (data) formatDataCounterpartyAndSetState(data);
  }, [data]);

  const onSubmit: SubmitHandler<FormValues> = (data) => {
    dispatch(removeCounterparty());
    dispatch(removeDocumentData());
    dispatch(removeMyCompany());
    dispatch(fetchCompany(data.code.trim()));
  };

  return (
    <section className="check-company">
      <div className="check-company__header">
        <h1 className="check-company__title _title">Перевірка контрагента</h1>
        <form onSubmit={handleSubmit(onSubmit)} className="check-company__form">
          <input
            type="text"
            className="check-company__input _input"
            placeholder="Введіть код ЄДРПОУ"
            {...register("code", { required: true, minLength: 8, maxLength: 10 })}
          />
          <button type="submit" className="check-company__button-check _button">
            Перевірити
          </button>
        </form>
      </div>

      {statusCheckedCompany === "success" ? (
        <>
          <NavPanel
            documenTitleToPrint={`${fullInfo.fullName.data}`}
            refComponentToPrint={companyInfo}
          />
          <section ref={companyInfo} className="check-company__content">
            <h2>
              {fullInfo.fullName.data} <br />
              (дані взято з OpendataUA)
            </h2>
            <ul>
              {Object.keys(fullInfo).map((key, index) => {
                return (
                  fullInfo[key].data && (
                    <li key={key + index}>
                      <div>{fullInfo[key].key}:</div>
                      <div>{fullInfo[key].data}</div>
                    </li>
                  )
                );
              })}
            </ul>
          </section>

          <div className="check-company__footer">
            <Link to="/contract">
              <button type="button" className="check-company__button-contract _button">
                Укласти договір
              </button>
            </Link>
          </div>
        </>
      ) : statusCheckedCompany === "loading" ? (
        <ProgressBar
          height="80"
          width="80"
          ariaLabel="progress-bar-loading"
          wrapperStyle={{}}
          wrapperClass="progress-bar-wrapper"
          borderColor="#F4442E"
          barColor="#51E5FF"
        />
      ) : statusCheckedCompany === "error" ? (
        <Error text={"Виникла помилка"} />
      ) : (
        ""
      )}
    </section>
  );
};

export default CheckCompany;
