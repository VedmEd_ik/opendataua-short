import React, { useState } from "react";
import cn from "classnames";
import { Navigate, useNavigate } from "react-router-dom";
import { getAuth, signInWithEmailAndPassword, sendPasswordResetEmail, User } from "firebase/auth";
import "./Home.scss";
import useAuth from "../hooks/use-auth";
import { setUser } from "src/redux/user/slice";
import { useAppDispatch } from "src/redux/store";
import { UserType } from "src/redux/user/types";
import { child, get, ref } from "firebase/database";
import { database } from "src/firebase";

const Home: React.FC = () => {
  const [email, setEmail] = useState<string>("");
  const [pass, setPass] = useState<string>("");
  const [passError, setPassError] = useState<boolean>(false);
  const [isResetPas, setIsResetPas] = useState<boolean | "isSend" | "send">(false); // false | isSend | send
  const dbRef = ref(database);

  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const auth = getAuth();
  const { isAuth } = useAuth();

  // Todo: перенести авторизацію в reduxThunk
  // Авторизація через firebase
  const handleLogin = (email: string, password: string) => {
    setPassError(false);

    signInWithEmailAndPassword(auth, email, password)
      .then((response) => {
        setUserObj(response.user);
        navigate("/check-company");
      })
      .catch((error) => {
        console.log(error);
        setPassError(true);
      });
  };
  // Todo: перенести отримання даних в reduxThunk
  // Запис даних про авторизованого користувача в redux і локальне сховище
  const setUserObj = async (userFromFirebase: User) => {
    try {
      const data = await get(child(dbRef, `/${userFromFirebase.uid}`));
      if (data.exists()) {
        const dataUser: UserType = {
          id: userFromFirebase.uid,
          email: userFromFirebase.email,
          name: userFromFirebase.displayName,
          ...data.val(),
        };
        window.localStorage.setItem("userData", JSON.stringify(dataUser));

        dispatch(setUser(dataUser));
      } else {
        console.log("No data available");
      }
    } catch (error) {
      console.error(error);
    }
  };

  const pressEnter = (event: React.KeyboardEvent) => {
    if (event.key === "Enter") {
      handleLogin(email, pass);
    }
  };

  // Скидання паролю
  const resetPassword = (email: string) => {
    sendPasswordResetEmail(auth, email);
  };

  // Попап скидання паролю
  const resetPasPopap = () => {
    switch (isResetPas) {
      case false:
        return (
          <section className="auth">
            <div className="auth__title auth__title_login _title">Авторизуватися</div>

            <form className="auth__form">
              <input
                value={email}
                onChange={(e) => {
                  setEmail(e.target.value);
                }}
                type="email"
                placeholder="Введіть email"
                className={cn("_input", { _error: passError })}
              />

              <input
                value={pass}
                onChange={(e) => {
                  setPass(e.target.value);
                }}
                onKeyDown={pressEnter}
                type="password"
                placeholder="Введіть пароль"
                className={cn("_input", { _error: passError })}
              />
              {passError ? (
                <div
                  className="auth__forgot-password"
                  onClick={() => {
                    setIsResetPas("isSend");
                  }}
                >
                  Забули пароль?
                </div>
              ) : (
                ""
              )}

              <button type="button" onClick={() => handleLogin(email, pass)} className="_button">
                Увійти
              </button>
            </form>
          </section>
        );
      case "isSend":
        return (
          <section className="reset-password-popap">
            <div
              className="reset-password-popap__back _icon-arrow-left-1"
              onClick={() => setIsResetPas(false)}
            >
              Назад
            </div>
            <h2 className="reset-password-popap__title">Скидання паролю</h2>
            <div className="reset-password-popap__text">
              Ми відправимо вам посилання для сидання паролю на ваш email
            </div>
            <input
              value={email}
              onKeyDown={pressEnter}
              onChange={(e) => {
                setEmail(e.target.value);
              }}
              type="email"
              placeholder="Введіть email"
              className="reset-password-popap__email _input"
            />
            <button
              type="button"
              onClick={() => {
                resetPassword(email);
                setIsResetPas("send");
              }}
              className="reset-password-popap__button _button"
            >
              Відправити
            </button>
          </section>
        );
      case "send":
        return (
          <section className="reset-password-popap">
            <h2 className="reset-password-popap__title">
              Посилання для скидання паролю відправлено вам на електронну адресу!
            </h2>
            <button
              type="button"
              className="reset-password-popap__button _button"
              onClick={() => setIsResetPas(false)}
            >
              Ок! Дякую!
            </button>
          </section>
        );
    }
  };

  if (isAuth) return <Navigate to="/check-company" replace={true} />;

  return <div className="auth-wrapper">{resetPasPopap()}</div>;
};

export default Home;
