import { BankDetailsType, PassportType } from "src/redux/counterparty/types";

//========= тип документу, на підставі якого діє підписант ============//
export type BasisDocumentType = {
  [key: string]: string;
};

//========= Тип області, де видавався паспорт ============//
export type RegionsPassportType = {
  [key: string]: string;
};

//========= Тип списку договорів ============//
export type DocumentsListType = { [key: string]: string };

//========= Типи даних моїх компаній ============//
export type FirmDataType = {
  [key: string]: MyCompanyType;
};

export type MyCompanyType = {
  fullName?: string;
  shortName?: string;
  address?: string;
  director?: string;
  code?: string;
  fullLegalForm?: string | null;
  shortLegalForm?: string | null;
  contacts?: {
    phone?: string | null;
    additionalPhone?: string | null;
    fax?: string | null;
    email?: string | null;
    webSite?: string | null;
    otherContacts?: string | null;
  };
  signer?: {
    name?: string | null;
    role?: string | null;
  };
  basisDocument?: string;
  bankDetails?: BankDetailsType;
  taxationSystem?: string | null;
  PDV?: string | null;
  IPN?: string | null;
  passport?: PassportType;
};

export type FirebaseDataType = {
  firmData: FirmDataType;
  documentsList: DocumentsListType;
  managers: string[];
};

//========= Тип строків оплати товару ============//
export type PaymentTermType = {
  [key: string]: string;
};

export type PassportTypeType = {
  book: string;
  id: string;
};

//========= Список менеджерів по роботі з клієнтами ============//
export type ManagersType = string[];

//========= Список торгових мереж ============//
export type RetailersType = string[];
