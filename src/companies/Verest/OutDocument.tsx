import React, { useEffect } from "react";
import { MeatContract, PetrolContract, LivestockContract, ServicesContract } from "./contracts";
import { PDFViewer } from "@react-pdf/renderer";
import { useSelector } from "react-redux";
import { documentDataSelector, fontSizeSelector } from "src/redux/document/selectors";
import { counterPartySelector } from "src/redux/counterparty/selectors";
import { myCompanySelector } from "src/redux/myCompany/selectors";
import "./documentStyles.scss";
import "../../styles/print.scss";
import { genQrCode } from "src/utils/functions";
import { MyCompanyType } from "src/@types/types";
import { NewCounterPartyType } from "src/redux/counterparty/types";
import { dataFromFormType } from "src/redux/document/types";

const document = (
  payment: string,
  myCompany: MyCompanyType,
  counterparty: NewCounterPartyType,
  dataDocument: dataFromFormType,
  fontSize: number,
  qrCode: string,
) => {
  console.log("Спрацювала функція document() в OutDocument");
  switch (dataDocument.documentName) {
    case "Договір поставки м'ясопродуктів":
      return (
        <PDFViewer key={dataDocument.documentName} className="iframe-pdf">
          <MeatContract
            payment={payment}
            myCompany={myCompany}
            counterparty={counterparty}
            dataDocument={dataDocument}
            fontSize={fontSize}
            qrCode={qrCode}
          />
        </PDFViewer>
      );

    case "Договір поставки нафтопродуктів":
      return (
        <PDFViewer key={dataDocument.documentName} className="iframe-pdf">
          <PetrolContract
            payment={payment}
            myCompany={myCompany}
            counterparty={counterparty}
            dataDocument={dataDocument}
            fontSize={fontSize}
            qrCode={qrCode}
          />
        </PDFViewer>
      );
    case "Договір поставки свиней, ВРХ":
      return (
        <PDFViewer key={dataDocument.documentName} className="iframe-pdf">
          <LivestockContract
            payment={payment}
            myCompany={myCompany}
            counterparty={counterparty}
            dataDocument={dataDocument}
            fontSize={fontSize}
            qrCode={qrCode}
          />
        </PDFViewer>
      );
    case "Договір про надання послуг":
      return (
        <PDFViewer key={dataDocument.documentName} className="iframe-pdf">
          <ServicesContract
            payment={payment}
            myCompany={myCompany}
            counterparty={counterparty}
            dataDocument={dataDocument}
            fontSize={fontSize}
            qrCode={qrCode}
          />
        </PDFViewer>
      );
  }
};

const OutDocumentVerest: React.FC = () => {
  const counterparty = useSelector(counterPartySelector);
  const dataDocument = useSelector(documentDataSelector);
  const myCompany = useSelector(myCompanySelector);
  const payment = dataDocument.documentPayment;
  const fontSize = useSelector(fontSizeSelector);

  const [qr, setQr] = React.useState(null);

  console.log("рендер компоненту OutDocumentVerest");

  useEffect(() => {
    genQrCode(
      dataDocument.documentName,
      dataDocument.documentNumber,
      dataDocument.documentDate,
      myCompany.shortName,
      counterparty.shortName,
      dataDocument.manager,
      dataDocument.retailer,
    ).then((val) => {
      setQr(val);
    });
  }, [
    dataDocument.documentName,
    dataDocument.documentNumber,
    dataDocument.documentDate,
    myCompany.shortName,
    counterparty.shortName,
    dataDocument.manager,
    dataDocument.retailer,
  ]);

  return (
    qr && (
      <div className="output-data__document document">
        {document(payment, myCompany, counterparty, dataDocument, fontSize, qr)}
      </div>
    )
  );
};

export default OutDocumentVerest;
