import { MyCompanyType } from "src/@types/types";
import { NewCounterPartyType } from "src/redux/counterparty/types";
import { dataFromFormType } from "src/redux/document/types";

export type ContractProps = {
  payment: string;
  myCompany: MyCompanyType;
  counterparty: NewCounterPartyType;
  dataDocument: dataFromFormType;
  fontSize: number;
  qrCode?: string;
};
