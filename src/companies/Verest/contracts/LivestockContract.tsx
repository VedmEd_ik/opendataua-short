import React from "react";
import { Page, Text, Document, View, Image, StyleSheet } from "@react-pdf/renderer";
import { capitalize, formatDate, parseNameDocument, paymentProcedure } from "src/utils/functions";
import { styles } from "../styles";
import { ContractProps } from "./types";

const LivestockContract: React.FC<ContractProps> = ({
  payment,
  myCompany,
  counterparty,
  dataDocument,
  fontSize,
  qrCode,
}) => {
  const { nameDocument, document } = parseNameDocument(dataDocument.documentName);

  const fontStyles = StyleSheet.create({
    mainStyles: {
      height: "100%",
      paddingTop: 30,
      paddingBottom: 40,
      paddingLeft: 50,
      paddingRight: 30,
      fontFamily: "Roboto",
      fontSize: fontSize,
    },
  });

  return (
    <Document>
      <Page style={fontStyles.mainStyles} size="A4">
        <View style={styles.wrap}>
          {/* Назва та номер договору */}
          <Text style={styles.title}>
            {document} № {dataDocument.documentNumber}
          </Text>
          <Text style={styles.title}>{nameDocument}</Text>
          {/* Місце та дата укладення */}
          <View style={styles.date}>
            <Text style={styles.text}>с.Гірчична</Text>
            <Text style={styles.text}>{formatDate(dataDocument.documentDate)} року</Text>
          </View>
          {/* Преамбула */}
          <Text style={styles.text}>
            <Text style={styles._bold}>{counterparty.fullName.toUpperCase()}</Text>, від імені якого
            діє {dataDocument.signer.role.toLowerCase()} {dataDocument.signer.name} (документ на
            підставі якого діє - {dataDocument.signerBasisDocument}) (надалі по тексту -
            "Постачальник"), та
          </Text>
          <Text style={[styles.text, styles._marginTop]}>
            <Text style={styles._bold}>{myCompany.fullName.toUpperCase()}</Text>, від імені якого
            діє {myCompany.signer.role.toLowerCase()} {myCompany.signer.name} (документ, на підставі
            якого він діє - {myCompany.basisDocument}) (надалі по тексту - "Покупець", разом -
            "Сторони", а кожна окремо - "Сторона"), уклали цей {dataDocument.documentName} №{" "}
            {dataDocument.documentNumber} (надалі по тексту - "Договір") про наступне:
          </Text>
          {/* 1. ПРЕДМЕТ ДОГОВОРУ */}
          <Text style={styles.subtitle}></Text>
          <Text style={[styles.text, styles.textItem]}>
            1.1. Згідно даного договору Постачальник зобов'язується поставляти Покупцеві поголів’я
            свиней, ВРХ (надалі - Товар), а Покупець зобов'язується приймати та своєчасно оплачувати
            його в порядку та на умовах, передбачених цим Договором.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            1.2. У разі проведення Покупцем миття та дезінфекції автотранспорту Продавця, яким було
            доставлено товар, останній зобов'язаний також оплатити надані послуги згідно із
            виставлених рахунків.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            1.3. Товар вимірюється: по вазі - в кілограмах, по кількості - в головах (шт.).
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            1.4. Кількість, вид тварин та їх вгодованість вказується в товаросупровідних документах
            та приймальній квитанції.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            1.5. Якість товару підтверджується ветеринарним свідоцтвом затвердженої форми, яке
            Постачальник при здійсненні поставки зобов'язаний передати Покупцеві.
          </Text>
          {/* 2. Порядок, умови поставки та прийому товару */}
          <Text style={styles.subtitle}>2. Порядок, умови поставки та прийому товару</Text>
          <Text style={[styles.text, styles.textItem]}>
            2.1. Доставка Товару за домовленістю (усною чи письмовою) може здійснюватися транспортом
            як Покупця так і Постачальника.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            2.2. Фактична вага поставленого Товару зазначається у приймальній квитанції за формою
            ПК-1, товарно-транспортній та видаткових накладних і не може бути змінена після
            підписання їх представником Покупця.
          </Text>
          <Text style={[styles.text, styles.textItem, styles._bold]}>
            2.3. Постачальник зобов’язується:
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            2.3.1. Поставляти Товар належної якості;
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            2.3.2. При передачі кожної партії товару надати Покупцеві оригінали наступних
            документів:
          </Text>
          <Text style={[styles.text, styles.textItem]}>2.3.2.1. товарно-транспортна накладна;</Text>
          <Text style={[styles.text, styles.textItem]}>
            2.3.2.2. ветеринарне свідоцтво форми №1;
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            2.3.2.3. накладна на переміщення тварин;
          </Text>
          <Text style={[styles.text, styles.textItem]}>2.3.2.4. видаткова накладна.</Text>
          <Text style={[styles.text, styles.textItem, styles._bold]}>
            2.4. Покупець зобов’язується:
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            2.2.1. Після здійснення приймання-передачі товару, але не пізніше наступного робочого
            дня, надати Постачальнику приймальну квитанцію.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            2.2.2. Завантаження товару на транспорт здійснюється силами та за рахунок Постачальника.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            2.2.3. Розвантаження товару на складі Покупця здійснюється силами та за рахунок Покупця.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            2.2.4. Приймання-передача товару по кількості та вазі здійснюється уповноваженими
            представниками Постачальника і Покупця у місці розвантаження товару на підставі
            товарно-транспортних накладних, після чого Покупцем складається приймальна квитанція за
            формою ПК-1.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            2.2.5. У разі якщо вага та кількість Товару, які зазначені в товарно-транспортній
            накладній не відповідають вазі та кількості фактично поставленого Товару, що зазначені в
            приймальній квитанції, Постачальник зобов’язаний на протязі 2-х робочих днів надати
            Покупцю виправлені товарно-транспортну та видаткові накладні.
          </Text>
          {/* 3. Ціна товару та порядок розрахунків */}
          <Text style={styles.subtitle}>3. Ціна товару та порядок розрахунків</Text>
          <Text style={[styles.text, styles.textItem]}>
            3.1. Ціна за кілограм товару встановлюється з врахуванням ПДВ та зазначається у рахунках
            та/або видаткових накладних.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            3.2. Оплата здійснюється на підставі приймальної квитанції, складеної за формою ПК-1.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            3.3. Покупець зобов'язаний оплатити Товар відповідно до рахунку та/або накладної{" "}
            <Text style={styles._bold}>{paymentProcedure(payment, "товар")}.</Text>
          </Text>
          {/* 4. Особливі умови */}
          <Text style={styles.subtitle}>4. Особливі умови</Text>
          <Text style={[styles.text, styles.textItem]}>
            4.1. Продавець гарантує, що він є виробником Товару, який є предметом Договору, також
            гарантує що товар не є проданий, переданий, заставлений третім особам, не знаходиться
            під арештом, судових справ та інших заборон щодо його відчуження немає, товар не є
            обтяженим ніякими зобов'язаннями, жодними претензіями третіх осіб.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            4.2. Право власності на Товар та ризик випадкової загибелі (псування, пошкодження )
            товару переходить до Покупця з моменту отримання Товару Покупцем.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            4.3. Сторони згодні з тим, що передача окремих прав і обов'язків та Договору в цілому
            третім особам допускається тільки за попередньою письмовою згодою обох Сторін за
            Договором.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            4.4. У випадку реорганізації будь-якої Сторони за Договором, правонаступник такої
            Сторони несе всі обов'язки і має всі права за цим Договором.
          </Text>
          {/* 5. Відповідальність Сторін */}
          <Text style={styles.subtitle}>5. Відповідальність Сторін</Text>
          <Text style={[styles.text, styles.textItem]}>
            5.1. Сторони несуть відповідальність за невиконання або неналежне виконання зобов'язань
            згідно чинного законодавства та умов даного Договору.
          </Text>
          {/* 6.	Форс-мажорні обставини */}
          <Text style={styles.subtitle}>6. Форс-мажорні обставини</Text>
          <Text style={[styles.text, styles.textItem]}>
            6.1. Сторони звільняються від відповідальності за часткове або повне невиконання
            зобов'язань по даному Договору, якщо це невиконання сталося як наслідок обставин
            непереборної сили, що виникли після укладання даного Договору як результат подій
            надзвичайного характеру, які Сторона не могла ні передбачити, ні відвернути розумними
            заходами. До обставин непереборної сили відносяться події, на які Сторона не може
            впливати і за виникнення яких не несе відповідальності (стихійні лиха, екстремальні
            погодні умови,пожежі, страйки,втручання з боку властей тощо).
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            6.2. Сторона, що посилається на обставини непереборної сили, зобов'язана у триденний
            строк з моменту виникнення таких обставин поінформувати іншу Сторону про їх настання у
            письмовій формі. На вимогу іншої сторони має бути наданий посвідчувальний документ,
            виданий Торгово-промисловою палатою (іншим уповноваженим органом) України. Інформація
            повинна містити дані про характер обставин, а також за можливістю оцінку їх впливу на
            виконання Сторонами своїх зобов'язань за даним Договором і на термін виконання
            зобов'язань.
          </Text>
          {/* 7.	Порядок вирішення спорів */}
          <Text style={styles.subtitle}>7. Порядок вирішення спорів</Text>
          <Text style={[styles.text, styles.textItem]}>
            7.1. Всі спори, що виникли під час виконання даного договору, вирішуються шляхом
            переговорів.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            7.2. У випадку не досягнення згоди Сторонами спір передається на розгляд до
            господарського суду у відповідності з законодавством України.
          </Text>
          {/* 8. Термін дії Договору */}
          <Text style={styles.subtitle}>8. Термін дії Договору</Text>
          <Text style={[styles.text, styles.textItem]}>
            8.1 Договір набуває чинності з моменту його підписання і скріплення печатками сторін і
            діє до кінця календарного року, в якому його було підписано сторонами.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            8.2 У випадку, якщо за 30 календарних днів до закінчення терміну дії Договору жодна із
            Сторін не заявила письмово про припинення його дії, Договір вважається продовженим
            автоматично на кожен наступний календарний рік на тих же умовах.
          </Text>
          <Text style={[styles.text, styles.textItem]}>8.3 Договір припиняє свою дію у разі:</Text>
          <Text style={[styles.text, styles.textItem]}>8.3.1. закінчення терміну його дії;</Text>
          <Text style={[styles.text, styles.textItem]}>
            8.3.2. припинення його дії за згодою обох Сторін;
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            8.3.3. розірвання його за вимогою однієї із Сторін, у порядку встановленому чинним
            законодавством України;
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            8.3.4. укладення нового договору стосовно такого ж предмету договору;
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            8.3.5. в інших випадках, передбачених чинним законодавством України.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            В будь-якому випадку Договір продовжує свою дію до моменту виконання Сторонами своїх
            зобов’язань, що виникли під час дії Договору.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            8.4 Підписанням цього Договору Сторони погоджуються, що усі Договори, предмет яких
            аналогічний предмету даного Договору та які були укладені до моменту набрання чинності
            цим Договором втрачають юридичну силу.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            8.5 Враховуючи положення ч. 2 ст. 207 Цивільного кодексу України та ст.181
            Господарського кодексу України Сторони погодили, що належно оформлені та підписані
            Сторонами копії цього Договору, рахунків-фактур, додаткових угод до нього, а також інших
            документів, що додаються до цього Договору і складають його невід’ємну частину, які
            передані між Сторонами за допомогою факсу, електронної пошти та іншими засобами
            телекомунікації, мають силу оригіналів до моменту обміну Сторонами належним чином
            оформленими оригіналами документів. Окрім цього, якщо вищезазначені документи були
            підписані електронним підписом (ЕЦП, КЕП тощо) уповноважених осіб та передані за
            допомогою засобів телекомунікації (спеціальних програм, електронної пошти тощо) , то
            вони також мають силу оригіналів.
          </Text>
          {/* 9.	Інші умови */}
          <Text style={styles.subtitle}>9. Інші умови</Text>
          <Text style={[styles.text, styles.textItem]}>
            9.1. У випадках, непередбачених цим Договором, Сторони керуються чинним законодавством
            України.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            9.2. Сторона несе повну відповідальність за правильність вказаних нею у цьому Договорі
            реквізитів та зобов'язується своєчасно у письмовій формі повідомляти іншу Сторону про їх
            зміну, а у разі неповідомлення несе ризик настання пов'язаних із ним несприятливих
            наслідків.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            9.3. Підписуючи цей Договір, Сторони надають згоду на використання та обробку їх
            персональних даних у порядку Закону України» Про захист персональних даних» з метою
            здійснення сторонами господарської діяльності. Підписуючи цей Договір, Сторони
            підтверджують, що були повідомлені про свої права згідно ст.8 Закону України» Про захист
            персональних даних», а також про мету збору персональних даних ( здійснення
            господарської діяльності) та про особу-контрагента, яким передаються їх персональні
            дані.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            9.4. Цей Договір складений у двох оригінальних примірниках, по одному для кожної зі
            Сторін.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            9.5. Якщо інше прямо не передбачено цим Договором або чинним в Україні законодавством,
            зміни у цей Договір можуть бути внесені тільки за домовленістю Сторін, яка оформлюється
            додатковою угодою до цього Договору.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            9.6. Зміни до цього Договору набирають чинності з моменту належного оформлення Сторонами
            відповідної додаткової угоди до цього Договору, якщо інше не встановлено у самій
            додатковій угоді, цьому Договорі або у чинному в Україні законодавстві.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            9.7. Якщо інше прямо не передбачено цим Договором або чинним в Україні законодавством,
            цей Договір може бути розірваний тільки за домовленістю Сторін, яка оформлюється
            додатковою угодою до цього Договору.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            9.8. Цей Договір вважається розірваним з моменту належного оформлення Сторонами
            відповідної додаткової угоди до цього Договору, якщо інше не встановлено у самій
            додатковій угоді, цьому Договорі або у чинному в Україні законодавстві.
          </Text>
          {/* 10. АДРЕСА, ПОДАТКОВИЙ СТАТУС, ПЛАТІЖНІ РЕКВІЗИТИ, ПІДПИСИ СТОРІН */}
          <Text style={styles.subtitle}>
            10. АДРЕСА, ПОДАТКОВИЙ СТАТУС, ПЛАТІЖНІ РЕКВІЗИТИ, ПІДПИСИ СТОРІН
          </Text>
          {/* Реквізити сторін */}
          <View style={styles.detailsContainer}>
            {/* Реквізити контрагента */}
            <View style={styles.detailsFirm}>
              <View style={styles.firmType}>
                <Text style={styles.detailTitle}>ПРОДАВЕЦЬ</Text>
              </View>
              <Text style={styles.detailTitle}>{counterparty.shortName.toUpperCase()}</Text>

              <View style={styles.detailsInfoBlock}>
                <Text style={styles.detailsSubtitle}>Юридична адреса:</Text>
                <Text style={styles.detailsText}>{counterparty.address}</Text>
              </View>

              <View>
                {dataDocument.postAddress && (
                  <View style={styles.detailsInfoBlock}>
                    <Text style={styles.detailsSubtitle}>Поштова адреса:</Text>
                    <Text style={styles.detailsText}>{dataDocument.postAddress}</Text>
                  </View>
                )}
              </View>

              <View style={styles.detailsInfoBlock}>
                <Text style={styles.detailsSubtitle}>Код ЄДРПОУ: </Text>
                <Text style={styles.detailsText}>{counterparty.code}</Text>
              </View>

              <View>
                {dataDocument.passport.status && (
                  <View style={styles.detailsInfoBlock}>
                    <Text style={styles.detailsSubtitle}>Паспорт: </Text>
                    {dataDocument.passport.passportType === "passportTypeBook" ? (
                      <Text>
                        {dataDocument.passport.number}, виданий {dataDocument.passport.organName} в{" "}
                        {dataDocument.passport.organRegion} {formatDate(dataDocument.passport.date)}{" "}
                        року
                      </Text>
                    ) : (
                      <Text>
                        ID-картка № {dataDocument.passport.number}, виданий{" "}
                        {formatDate(dataDocument.passport.date)} року органом{" "}
                        {dataDocument.passport.organName}
                      </Text>
                    )}
                  </View>
                )}
              </View>

              <View>
                {dataDocument.isTax && (
                  <View style={styles.detailsInfoBlock}>
                    <Text style={styles.detailsSubtitle}>Система оподаткування: </Text>
                    <Text style={styles.detailsText}>
                      {dataDocument.taxationSystem} {dataDocument.PDV}
                    </Text>
                  </View>
                )}
              </View>

              <View>
                {dataDocument.IPN && (
                  <View style={styles.detailsInfoBlock}>
                    <Text style={styles.detailsSubtitle}>ІПН: </Text>
                    <Text style={styles.detailsText}>{dataDocument.IPN}</Text>
                  </View>
                )}
              </View>

              <View>
                {(dataDocument.phones || dataDocument.phonesAdd) && (
                  <View style={styles.detailsInfoBlock}>
                    <Text style={styles.detailsSubtitle}>Тел.: </Text>
                    <Text style={styles.detailsText}>
                      {dataDocument.phones}, {dataDocument.phonesAdd}
                    </Text>
                  </View>
                )}
              </View>

              <View>
                {(dataDocument.email || dataDocument.emailAdd) && (
                  <View style={styles.detailsInfoBlock}>
                    <Text style={styles.detailsSubtitle}>E-mail:</Text>
                    <Text style={styles.detailsText}>
                      {dataDocument.email},{dataDocument.emailAdd}
                    </Text>
                  </View>
                )}
              </View>

              <View>
                {dataDocument.bankDetails && (
                  <>
                    {dataDocument.bankDetails.bank1 &&
                      dataDocument.bankDetails.bank1["bank-account"] &&
                      dataDocument.bankDetails.bank1.bank && (
                        <View style={styles.detailsInfoBlock}>
                          <View style={styles.bankAccount}>
                            <Text style={styles.detailsSubtitle}>IBAN: </Text>
                            <Text style={styles.detailsText}>
                              {dataDocument.bankDetails.bank1["bank-account"]} в{" "}
                              {dataDocument.bankDetails.bank1.bank}
                            </Text>
                          </View>
                        </View>
                      )}
                    {dataDocument.bankDetails.bank2 &&
                      dataDocument.bankDetails.bank2["bank-account"] &&
                      dataDocument?.bankDetails?.bank2.bank && (
                        <View style={styles.detailsInfoBlock}>
                          <View style={styles.bankAccount}>
                            <Text style={styles.detailsSubtitle}>IBAN: </Text>
                            <Text style={styles.detailsText}>
                              {dataDocument.bankDetails.bank2["bank-account"]} в{" "}
                              {dataDocument.bankDetails.bank2.bank}
                            </Text>
                          </View>
                        </View>
                      )}
                  </>
                )}
              </View>
            </View>

            {/* Реквізити моєї компанії */}
            <View style={styles.detailsFirm}>
              <View style={styles.firmType}>
                <Text style={styles.detailTitle}>ПОКУПЕЦЬ</Text>
              </View>
              <Text style={styles.detailTitle}>{myCompany.shortName.toUpperCase()}</Text>

              <View style={styles.detailsInfoBlock}>
                <Text style={styles.detailsSubtitle}>Юридична/фактична адреса:</Text>
                <Text style={styles.detailsText}>{myCompany.address}</Text>
              </View>

              <View style={styles.detailsInfoBlock}>
                <Text style={styles.detailsSubtitle}>Код ЄДРПОУ: </Text>
                <Text style={styles.detailsText}>{myCompany.code}</Text>
              </View>
              {myCompany.passport.status && (
                <View style={styles.detailsInfoBlock}>
                  <Text style={styles.detailsSubtitle}>Паспорт: </Text>
                  {myCompany.passport.passportType === "passportTypeBook" ? (
                    <Text>
                      {myCompany.passport.number}, виданий {myCompany.passport.organName} в{" "}
                      {myCompany.passport.organRegion} {formatDate(myCompany.passport.date)} року
                    </Text>
                  ) : (
                    <Text>
                      ID-картка № {myCompany.passport.number}, виданий{" "}
                      {formatDate(myCompany.passport.date)} року органом{" "}
                      {myCompany.passport.organName}
                    </Text>
                  )}
                </View>
              )}

              {myCompany.taxationSystem && (
                <View style={styles.detailsInfoBlock}>
                  <Text style={styles.detailsSubtitle}>Система оподаткування: </Text>
                  <Text style={styles.detailsText}>
                    {myCompany.taxationSystem} {myCompany.PDV}
                  </Text>
                </View>
              )}

              {myCompany.IPN && (
                <View style={styles.detailsInfoBlock}>
                  <Text style={styles.detailsSubtitle}>ІПН: </Text>
                  <Text style={styles.detailsText}>{myCompany.IPN}</Text>
                </View>
              )}

              {myCompany.contacts.phone && (
                <View style={styles.detailsInfoBlock}>
                  <Text style={styles.detailsSubtitle}>Тел.: </Text>
                  <Text style={styles.detailsText}>{myCompany.contacts.phone}</Text>
                </View>
              )}

              {myCompany.contacts.email && (
                <View style={styles.detailsInfoBlock}>
                  <Text style={styles.detailsSubtitle}>E-mail:</Text>
                  <Text style={styles.detailsText}>{myCompany.contacts.email}</Text>
                </View>
              )}

              {myCompany.bankDetails && (
                <>
                  {myCompany.bankDetails.bank1 &&
                    myCompany.bankDetails.bank1["bank-account"] &&
                    myCompany.bankDetails.bank1.bank && (
                      <View style={styles.detailsInfoBlock}>
                        <View style={styles.bankAccount}>
                          <Text style={styles.detailsSubtitle}>IBAN: </Text>
                          <Text style={styles.detailsText}>
                            {myCompany.bankDetails.bank1["bank-account"]} в{" "}
                            {myCompany.bankDetails.bank1.bank}
                          </Text>
                        </View>
                      </View>
                    )}
                  {myCompany.bankDetails.bank2 &&
                    myCompany.bankDetails.bank2["bank-account"] &&
                    myCompany.bankDetails.bank2.bank && (
                      <View style={styles.detailsInfoBlock}>
                        <View style={styles.bankAccount}>
                          <Text style={styles.detailsSubtitle}>IBAN: </Text>
                          <Text style={styles.detailsText}>
                            {myCompany.bankDetails.bank2["bank-account"]} в{" "}
                            {myCompany.bankDetails.bank2.bank}
                          </Text>
                        </View>
                      </View>
                    )}
                </>
              )}
            </View>
          </View>
          {/* Підписи сторін */}
          <View style={[styles.detailsSignatures, styles._marginTop]}>
            {/* Підпис контрагента */}
            <View style={styles.detailsSignature}>
              <Text style={styles.text}>{capitalize(dataDocument.signer.role)}</Text>
              <Text style={styles.text}> </Text>
              <Text style={[styles.text, styles.detailsSignatureRole, styles._underLine]}>
                {dataDocument.signer.name}
              </Text>
            </View>

            {/* Підпис мого керівника */}
            <View style={styles.detailsSignature}>
              <Text style={styles.text}>{capitalize(myCompany.signer.role)}</Text>
              <Text style={styles.text}> </Text>
              <Text style={[styles.text, styles.detailsSignatureRole, styles._underLine]}>
                {myCompany.signer.name}
              </Text>
            </View>
          </View>
        </View>
        {/* Нижній колонтитул */}
        <View style={styles.footerContainer} fixed>
          <Text>Постачальник ______________</Text>
          <Text
            style={styles.pageNumber}
            render={({ pageNumber, totalPages }) => `Сторінка ${pageNumber} з ${totalPages}`}
          />
          <Text>Покупець ______________</Text>
        </View>
        {/* QR-код */}
        <View style={styles.qrCode}>
          <Image src={qrCode} style={styles.qrCodeImg} cache={false}></Image>
        </View>
      </Page>
    </Document>
  );
};

export default LivestockContract;
