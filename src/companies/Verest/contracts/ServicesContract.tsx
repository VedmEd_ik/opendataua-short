import React from "react";
import { Page, Text, Document, View, Image, StyleSheet } from "@react-pdf/renderer";
import { capitalize, formatDate, parseNameDocument, paymentProcedure } from "src/utils/functions";
import { styles } from "../styles";
import { ContractProps } from "./types";

const ServicesContract: React.FC<ContractProps> = ({
  payment,
  myCompany,
  counterparty,
  dataDocument,
  fontSize,
  qrCode,
}) => {
  const { nameDocument, document } = parseNameDocument(dataDocument.documentName);

  const fontStyles = StyleSheet.create({
    mainStyles: {
      height: "100%",
      paddingTop: 30,
      paddingBottom: 40,
      paddingLeft: 50,
      paddingRight: 30,
      fontFamily: "Roboto",
      fontSize: fontSize,
    },
  });

  return (
    <Document>
      <Page style={fontStyles.mainStyles} size="A4">
        <View style={styles.wrap}>
          {/* Назва та номер договору */}
          <Text style={styles.title}>
            {document} № {dataDocument.documentNumber}
          </Text>
          <Text style={styles.title}>{nameDocument}</Text>
          {/* Місце та дата укладення */}
          <View style={styles.date}>
            <Text style={styles.text}>с.Гірчична</Text>
            <Text style={styles.text}>{formatDate(dataDocument.documentDate)} року</Text>
          </View>
          {/* Преамбула */}

          <Text style={[styles.text, styles._marginTop]}>
            <Text style={styles._bold}>{counterparty.fullName.toUpperCase()}</Text>, від імені якого
            діє {dataDocument.signer.role.toLowerCase()} {dataDocument.signer.name} (документ, на
            підставі якого він діє - {dataDocument.signerBasisDocument}) (надалі по тексту -
            "Замовник"), та
          </Text>

          <Text style={styles.text}>
            <Text style={styles._bold}>{myCompany.fullName.toUpperCase()}</Text>, від імені якого
            діє {myCompany.signer.role.toLowerCase()} {myCompany.signer.name} (документ на підставі
            якого діє - {myCompany.basisDocument}) (надалі по тексту - "Виконавець", разом -
            "Сторони", а кожна окремо - "Сторона"), уклали цей {dataDocument.documentName} №{" "}
            {dataDocument.documentNumber} (надалі по тексту - "Договір") про наступне:
          </Text>
          {/* 1. ПРЕДМЕТ ДОГОВОРУ */}
          <Text style={styles.subtitle}>1. ПРЕДМЕТ ДОГОВОРУ</Text>
          <Text style={[styles.text, styles.textItem]}>
            1.1. Виконавець протягом дії цього договору надає Замовнику послуги (в т.ч. послуги
            автокрану, екскаватора, асенізатора, послуги з перевезення вантажів тощо), а Замовник
            зобов’язується прийняти та оплатити надані послуги.{" "}
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            1.2. Замовник зобов’язаний забезпечити Виконавця усім необхідним для виконання роботи,
            передбаченої цим Договором.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            1.3. Замовник зобов’язаний своєчасно прийняти і оплатити роботу.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            1.4. Здавання послуг Виконавцем та приймання їх результатів Замовником оформлюється
            Актом приймання-передачі наданих послуг, який підписується повноважними представниками
            Сторін протягом 3 (трьох) робочих днів після фактичного надання послуг.
          </Text>
          {/* 2.	РОЗМІР І ПОРЯДОК ОПЛАТИ */}
          <Text style={styles.subtitle}>2. РОЗМІР І ПОРЯДОК ОПЛАТИ</Text>
          <Text style={[styles.text, styles.textItem]}>
            2.1. За виконану роботу Замовник сплачує Виконавцю вартість наданих послуг відповідно до
            виставлених рахунків та актів наданих послуг (виконаних робіт).
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            2.2. Розрахунки за цим Договором здійснюється в готівковій або безготівковій формі, або
            шляхом зарахування однорідних зустрічних вимог за іншими договорами.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            2.3. Замовник зобов'язаний оплатити замовлені послуги відповідно до рахунку та/або акту
            наданих послуг <Text style={styles._bold}>{paymentProcedure(payment, "послуги")}.</Text>
          </Text>

          {/* 3.	ВІДПОВІДАЛЬНІСТЬ СТОРІН */}
          <Text style={styles.subtitle}>3. ВІДПОВІДАЛЬНІСТЬ СТОРІН</Text>
          <Text style={[styles.text, styles.textItem]}>
            3.1. Сторони несуть відповідальність за невиконання або неналежне виконання покладених
            на них зобов’язань згідно з чинним законодавством України.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            3.2. Порушенням Договору є його невиконання або неналежне виконання, тобто виконання з
            порушенням умов, визначених змістом цього Договору.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            3.3. Сторона не несе відповідальності за порушення Договору, якщо воно сталося не з її
            вини (умислу чи необережності).
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            3.4. Сторона вважається невинуватою і не несе відповідальності за порушення Договору,
            якщо вона доведе, що вжила всіх залежних від неї заходів щодо належного виконання цього
            Договору.
          </Text>

          {/* 4.	ІНШІ УМОВИ ДОГОВОРУ */}
          <Text style={styles.subtitle}>4. ІНШІ УМОВИ ДОГОВОРУ</Text>
          <Text style={[styles.text, styles.textItem]}>
            4.1. Договір набуває чинності з моменту його підписання і скріплення печатками сторін і
            діє до кінця календарного року, в якому його було підписано сторонами. У випадку, якщо
            за 30 календарних днів до закінчення терміну дії Договору жодна із Сторін не заявила
            письмово про припинення його дії, Договір вважається продовженим автоматично на кожен
            наступний календарний рік на тих же умовах.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            4.2. Договір припиняє свою дію у разі:{" "}
          </Text>
          <Text style={[styles.text, styles.textItem]}>4.2.1. закінчення терміну його дії;</Text>
          <Text style={[styles.text, styles.textItem]}>
            4.2.2. припинення його дії за згодою обох Сторін;
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            4.2.3. розірвання його за вимогою однієї із Сторін, у порядку встановленому чинним
            законодавством України;
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            4.2.4. укладення нового договору стосовно такого ж предмету;
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            4.2.5. в інших випадках, передбачених чинним законодавством України.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            4.3. В будь-якому випадку Договір продовжує свою дію до моменту виконання Сторонами
            своїх зобов’язань, що виникли під час дії Договору.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            4.4. Підписанням цього Договору Сторони погоджуються, що усі Договори, предмет яких
            аналогічний предмету даного Договору та які були укладені до моменту набрання чинності
            цим Договором втрачають юридичну силу.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            4.5. Враховуючи положення ч. 2 ст. 207 Цивільного кодексу України та ст.181
            Господарського кодексу України Сторони погодили, що копії цього Договору,
            рахунків-фактур, додаткових угод до нього, а також інших документів, що додаються до
            цього Договору і складають його невід’ємну частину, які передані між Сторонами за
            допомогою факсу, електронної пошти та іншими засобами, мають силу оригіналів до моменту
            обміну Сторонами належним чином оформленими оригіналами документів.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            4.6. Усі спори, що виникають з цього Договору або пов'язані із ним, вирішуються шляхом
            переговорів між Сторонами. Якщо відповідний спір неможливо вирішити шляхом переговорів,
            він вирішується в судовому порядку за встановленою підвідомчістю та підсудністю такого
            спору відповідно до чинного законодавства України.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            4.7. Усі правовідносини, що виникають з цього Договору або пов'язані із ним, у тому
            числі пов'язані із дійсністю, укладенням, виконанням, зміною та припиненням цього
            Договору, тлумаченням його умов, визначенням наслідків недійсності або порушення
            Договору, регулюються цим Договором та відповідними нормами чинного законодавства
            України, а також звичаями ділового обороту, які застосовуються до таких правовідносин на
            підставі принципів добросовісності, розумності та справедливості.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            4.8. Сторони несуть повну відповідальність за правильність вказаних ними у цьому
            Договорі реквізитів та зобов'язуються своєчасно у письмовій формі повідомляти іншу
            Сторону про їх зміну, а у разі неповідомлення несуть ризик настання пов'язаних із ним
            несприятливих наслідків.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            4.9. Додаткові угоди та додатки до цього Договору є його невід'ємною частиною і мають
            юридичну силу у разі, якщо вони викладені у письмовій формі, підписані Сторонами та
            скріплені їх печатками.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            4.10. Цей Договір складений при повному розумінні Сторонами його умов та термінології
            українською мовою у двох автентичних примірниках, які мають однакову юридичну силу, - по
            одному для кожної із Сторін.
          </Text>

          {/* 5. АДРЕСА, ПОДАТКОВИЙ СТАТУС, ПЛАТІЖНІ РЕКВІЗИТИ, ПІДПИСИ СТОРІН */}
          <Text style={styles.subtitle}>
            5. АДРЕСА, ПОДАТКОВИЙ СТАТУС, ПЛАТІЖНІ РЕКВІЗИТИ, ПІДПИСИ СТОРІН
          </Text>
          {/* Реквізити сторін */}
          <View style={styles.detailsContainer}>
            {/* Реквізити контрагента */}
            <View style={styles.detailsFirm}>
              <View style={styles.firmType}>
                <Text style={styles.detailTitle}>ЗАМОВНИК</Text>
              </View>

              <Text style={styles.detailTitle}>{counterparty.shortName.toUpperCase()}</Text>

              <View style={styles.detailsInfoBlock}>
                <Text style={styles.detailsSubtitle}>Юридична адреса:</Text>
                <Text style={styles.detailsText}>{counterparty.address}</Text>
              </View>

              <View>
                {dataDocument.postAddress && (
                  <View style={styles.detailsInfoBlock}>
                    <Text style={styles.detailsSubtitle}>Поштова адреса:</Text>
                    <Text style={styles.detailsText}>{dataDocument.postAddress}</Text>
                  </View>
                )}
              </View>

              <View style={styles.detailsInfoBlock}>
                <Text style={styles.detailsSubtitle}>Код ЄДРПОУ: </Text>
                <Text style={styles.detailsText}>{counterparty.code}</Text>
              </View>

              <View>
                {dataDocument.passport.status && (
                  <View style={styles.detailsInfoBlock}>
                    <Text style={styles.detailsSubtitle}>Паспорт: </Text>
                    {dataDocument.passport.passportType === "passportTypeBook" ? (
                      <Text>
                        {dataDocument.passport.number}, виданий {dataDocument.passport.organName} в{" "}
                        {dataDocument.passport.organRegion} {formatDate(dataDocument.passport.date)}{" "}
                        року
                      </Text>
                    ) : (
                      <Text>
                        ID-картка № {dataDocument.passport.number}, виданий{" "}
                        {formatDate(dataDocument.passport.date)} року органом{" "}
                        {dataDocument.passport.organName}
                      </Text>
                    )}
                  </View>
                )}
              </View>

              <View>
                {dataDocument.isTax && (
                  <View style={styles.detailsInfoBlock}>
                    <Text style={styles.detailsSubtitle}>Система оподаткування: </Text>
                    <Text style={styles.detailsText}>
                      {dataDocument.taxationSystem} {dataDocument.PDV}
                    </Text>
                  </View>
                )}
              </View>

              <View>
                {dataDocument.IPN && (
                  <View style={styles.detailsInfoBlock}>
                    <Text style={styles.detailsSubtitle}>ІПН: </Text>
                    <Text style={styles.detailsText}>{dataDocument.IPN}</Text>
                  </View>
                )}
              </View>

              <View>
                {(dataDocument.phones || dataDocument.phonesAdd) && (
                  <View style={styles.detailsInfoBlock}>
                    <Text style={styles.detailsSubtitle}>Тел.: </Text>
                    <Text style={styles.detailsText}>
                      {dataDocument.phones}, {dataDocument.phonesAdd}
                    </Text>
                  </View>
                )}
              </View>

              <View>
                {(dataDocument.email || dataDocument.emailAdd) && (
                  <View style={styles.detailsInfoBlock}>
                    <Text style={styles.detailsSubtitle}>E-mail:</Text>
                    <Text style={styles.detailsText}>
                      {dataDocument.email},{dataDocument.emailAdd}
                    </Text>
                  </View>
                )}
              </View>

              <View>
                {dataDocument.bankDetails && (
                  <>
                    {dataDocument.bankDetails.bank1 &&
                      dataDocument.bankDetails.bank1["bank-account"] &&
                      dataDocument.bankDetails.bank1.bank && (
                        <View style={styles.detailsInfoBlock}>
                          <View style={styles.bankAccount}>
                            <Text style={styles.detailsSubtitle}>IBAN: </Text>
                            <Text style={styles.detailsText}>
                              {dataDocument.bankDetails.bank1["bank-account"]} в{" "}
                              {dataDocument.bankDetails.bank1.bank}
                            </Text>
                          </View>
                        </View>
                      )}
                    {dataDocument.bankDetails.bank2 &&
                      dataDocument.bankDetails.bank2["bank-account"] &&
                      dataDocument?.bankDetails?.bank2.bank && (
                        <View style={styles.detailsInfoBlock}>
                          <View style={styles.bankAccount}>
                            <Text style={styles.detailsSubtitle}>IBAN: </Text>
                            <Text style={styles.detailsText}>
                              {dataDocument.bankDetails.bank2["bank-account"]} в{" "}
                              {dataDocument.bankDetails.bank2.bank}
                            </Text>
                          </View>
                        </View>
                      )}
                  </>
                )}
              </View>
            </View>

            {/* Реквізити моєї компанії */}
            <View style={styles.detailsFirm}>
              <View style={styles.firmType}>
                <Text style={styles.detailTitle}>ВИКОНАВЕЦЬ</Text>
              </View>
              <Text style={styles.detailTitle}>{myCompany.shortName.toUpperCase()}</Text>

              <View style={styles.detailsInfoBlock}>
                <Text style={styles.detailsSubtitle}>Юридична адреса:</Text>
                <Text style={styles.detailsText}>{counterparty.address}</Text>
              </View>
              {dataDocument.postAddress && (
                <View style={styles.detailsInfoBlock}>
                  <Text style={styles.detailsSubtitle}>Поштова адреса:</Text>
                  <Text style={styles.detailsText}>{dataDocument.postAddress}</Text>
                </View>
              )}

              <View style={styles.detailsInfoBlock}>
                <Text style={styles.detailsSubtitle}>Код ЄДРПОУ: </Text>
                <Text style={styles.detailsText}>{myCompany.code}</Text>
              </View>
              {myCompany.passport.status && (
                <View style={styles.detailsInfoBlock}>
                  <Text style={styles.detailsSubtitle}>Паспорт: </Text>
                  {myCompany.passport.passportType === "passportTypeBook" ? (
                    <Text>
                      {myCompany.passport.number}, виданий {myCompany.passport.organName} в{" "}
                      {myCompany.passport.organRegion} {formatDate(myCompany.passport.date)} року
                    </Text>
                  ) : (
                    <Text>
                      ID-картка № {myCompany.passport.number}, виданий{" "}
                      {formatDate(myCompany.passport.date)} року органом{" "}
                      {myCompany.passport.organName}
                    </Text>
                  )}
                </View>
              )}

              {myCompany.taxationSystem ? (
                <View style={styles.detailsInfoBlock}>
                  <Text style={styles.detailsSubtitle}>Система оподаткування: </Text>
                  <Text style={styles.detailsText}>
                    {myCompany.taxationSystem} {myCompany.PDV}
                  </Text>
                </View>
              ) : (
                ""
              )}

              {myCompany.IPN && (
                <View style={styles.detailsInfoBlock}>
                  <Text style={styles.detailsSubtitle}>ІПН: </Text>
                  <Text style={styles.detailsText}>{myCompany.IPN}</Text>
                </View>
              )}

              {myCompany.contacts.phone && (
                <View style={styles.detailsInfoBlock}>
                  <Text style={styles.detailsSubtitle}>Тел.: </Text>
                  <Text style={styles.detailsText}>{myCompany.contacts.phone}</Text>
                </View>
              )}

              {myCompany.contacts.email && (
                <View style={styles.detailsInfoBlock}>
                  <Text style={styles.detailsSubtitle}>E-mail:</Text>
                  <Text style={styles.detailsText}>{myCompany.contacts.email}</Text>
                </View>
              )}

              {myCompany.bankDetails && (
                <>
                  {myCompany.bankDetails.bank1 &&
                    myCompany.bankDetails.bank1["bank-account"] &&
                    myCompany.bankDetails.bank1.bank && (
                      <View style={styles.detailsInfoBlock}>
                        <View style={styles.bankAccount}>
                          <Text style={styles.detailsSubtitle}>IBAN: </Text>
                          <Text style={styles.detailsText}>
                            {myCompany.bankDetails.bank1["bank-account"]} в{" "}
                            {myCompany.bankDetails.bank1.bank}
                          </Text>
                        </View>
                      </View>
                    )}
                  {myCompany.bankDetails.bank2 &&
                    myCompany.bankDetails.bank2["bank-account"] &&
                    myCompany.bankDetails.bank2.bank && (
                      <View style={styles.detailsInfoBlock}>
                        <View style={styles.bankAccount}>
                          <Text style={styles.detailsSubtitle}>IBAN: </Text>
                          <Text style={styles.detailsText}>
                            {myCompany.bankDetails.bank2["bank-account"]} в{" "}
                            {myCompany.bankDetails.bank2.bank}
                          </Text>
                        </View>
                      </View>
                    )}
                </>
              )}
            </View>
          </View>
          {/* Підписи сторін */}
          <View style={[styles.detailsSignatures, styles._marginTop]}>
            {/* Підпис контрагента */}
            <View style={styles.detailsSignature}>
              <Text style={styles.text}>{capitalize(dataDocument.signer.role)}</Text>
              <Text style={styles.text}> </Text>
              <Text style={[styles.text, styles.detailsSignatureRole, styles._underLine]}>
                {dataDocument.signer.name}
              </Text>
            </View>

            {/* Підпис мого керівника */}
            <View style={styles.detailsSignature}>
              <Text style={styles.text}>{capitalize(myCompany.signer.role)}</Text>
              <Text style={styles.text}> </Text>
              <Text style={[styles.text, styles.detailsSignatureRole, styles._underLine]}>
                {myCompany.signer.name}
              </Text>
            </View>
          </View>
        </View>
        {/* Нижній колонтитул */}
        <View style={styles.footerContainer} fixed>
          <Text>Замовник ______________</Text>
          <Text
            style={styles.pageNumber}
            render={({ pageNumber, totalPages }) => `Сторінка ${pageNumber} з ${totalPages}`}
          />
          <Text>Виконавець ______________</Text>
        </View>
        {/* QR-код */}
        <View style={styles.qrCode}>
          <Image src={qrCode} style={styles.qrCodeImg} cache={false}></Image>
        </View>
      </Page>
    </Document>
  );
};

export default ServicesContract;
