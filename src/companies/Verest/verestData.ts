import { DocumentsListType, FirmDataType } from "src/@types/types";

//Todo: перенести ці дані в firebase

//========= Дані моїх компаній ============//
export const firmData: FirmDataType = {
  Верест: {
    fullName: 'Товариство з обмеженою відповідальністю "Верест"',
    shortName: 'ТОВ "Верест"',
    address: "32400, Хмельницька обл., Кам'янець-Подільський район, с.Гірчична, вул.Центральна, 16",
    director: "Страхоцінський Віктор Вікторович",
    code: "23846567",
    fullLegalForm: "Товариство з обмеженою відповідальністю",
    shortLegalForm: "ТОВ",
    contacts: {
      phone: "(03858) 9-62-45",
      additionalPhone: "(067) 247-47-75",
      fax: "",
      email: "verest.tov@gmail.com",
      webSite: "verest.com.ua",
      otherContacts: "",
    },
    signer: {
      name: "Страхоцінський Віктор Вікторович",
      role: "Директор",
    },
    basisDocument: "Статут",
    bankDetails: {
      bank1: {
        "bank-account": "UA503052990000026007026003117",
        bank: 'АТ КБ "ПРИВАТБАНК" ',
      },
      bank2: {
        "bank-account": "UA973157840000026009316610558",
        bank: 'АТ "Ощадбанк"',
      },
    },
    taxationSystem: "Платник податку на прибуток на загальній системі",
    PDV: "з реєстрацією ПДВ",
    IPN: "238465622076",
    passport: {
      status: "",
      number: "",
      date: "",
      organName: "",
      organRegion: "",
    },
  },
  "Бернашевський Олександр Вячеславович": {
    fullName: "Фізична особа-підприємець Бернашевський Олександр Вячеславович",
    shortName: "ФОП Бернашевський Олександр Вячеславович",
    address: "32400, Хмельницька обл., Кам'янець-Подільський район, м. Дунаївці, вул. Червона, 33",
    director: "Бернашевський Олександр Вячеславович",
    code: "2290412394",
    fullLegalForm: "Фізична особа-підприємець",
    shortLegalForm: "ФОП",
    contacts: {
      phone: "(03858) 9-62-45",
      additionalPhone: "",
      fax: "",
      email: "",
      webSite: "",
      otherContacts: "",
    },
    signer: {
      name: "Бернашевський Олександр Вячеславович",
      role: "Керівник",
    },
    basisDocument: "Виписка з ЄДР",
    bankDetails: {
      bank1: {
        "bank-account": "UA763052990000026007006007263",
        bank: 'АТ КБ "ПРИВАТБАНК" ',
      },
    },
    taxationSystem: "Платник єдиного податку",
    PDV: "без реєстрації ПДВ",
    IPN: "",
    passport: {
      status: "",
      number: "",
      date: "",
      organName: "",
      organRegion: "",
    },
  },
  "Страхоцінський Віктор Вікторович": {
    fullName: "Фізична особа-підприємець Страхоцінський Віктор Вікторович",
    shortName: "ФОП Страхоцінський Віктор Вікторович",
    address:
      "Україна, 32400, Хмельницька обл., Кам'янець-Подільський р-н, м. Дунаївці, вул. Миколи Іщенка, буд. 26",
    director: "Страхоцінський Віктор Вікторович",
    code: "2605010896",
    fullLegalForm: "Фізична особа-підприємець",
    shortLegalForm: "ФОП",
    contacts: {
      phone: "(03858) 9-62-45",
      additionalPhone: "",
      fax: "",
      email: "",
      webSite: "",
      otherContacts: "",
    },
    signer: {
      name: "Страхоцінський Віктор Вікторович",
      role: "Керівник",
    },
    basisDocument: "Виписка з ЄДР",
    bankDetails: {
      bank1: {
        "bank-account": "UA643052990000026005036008580",
        bank: 'АТ КБ "ПРИВАТБАНК" ',
      },
    },
    taxationSystem: "Платник єдиного податку",
    PDV: "без реєстрації ПДВ",
    IPN: "",
    passport: {
      status: "",
      number: "",
      date: "",
      organName: "",
      organRegion: "",
    },
  },
  "Бернашевський Ярослав Олександрович": {
    fullName: "Фізична особа-підприємець Бернашевський Ярослав Олександрович",
    shortName: "ФОП Бернашевський Ярослав Олександрович",
    address:
      "32400, Хмельницька обл., Кам'янець-Подільський р-н, м. Дунаївці, вул. Червона, буд. 35",
    director: "Бернашевський Ярослав Олександрович",
    code: "3545903937",
    fullLegalForm: "Фізична особа-підприємець",
    shortLegalForm: "ФОП",
    contacts: {
      phone: "(03858) 9-62-45",
      additionalPhone: "",
      fax: "",
      email: "",
      webSite: "",
      otherContacts: "",
    },
    signer: {
      name: "Бернашевський Ярослав Олександрович",
      role: "Керівник",
    },
    basisDocument: "Виписка з ЄДР",
    bankDetails: {
      bank1: {
        "bank-account": "UA363154050000026006052418331",
        bank: 'АТ КБ "ПРИВАТБАНК" ',
      },
    },
    taxationSystem: "Платник єдиного податку",
    PDV: "без реєстрації ПДВ",
    IPN: "",
    passport: {
      status: "",
      number: "",
      date: "",
      organName: "",
      organRegion: "",
    },
  },
  "Олійник Денис Олександрович": {
    fullName: "Фізична особа-підприємець Олійник Денис Олександрович",
    shortName: "ФОП Олійник Денис Олександрович",
    address:
      "32400, Хмельницька обл., Кам’янець-Подільський район, м. Дунаївці, вул. Озерна, буд.35",
    director: "Олійник Денис Олександрович",
    code: "3274318195",
    fullLegalForm: "Фізична особа-підприємець",
    shortLegalForm: "ФОП",
    contacts: {
      phone: "(03858) 9-62-45",
      additionalPhone: "",
      fax: "",
      email: "",
      webSite: "",
      otherContacts: "",
    },
    signer: {
      name: "Олійник Денис Олександрович",
      role: "Керівник",
    },
    basisDocument: "Виписка з ЄДР",
    bankDetails: {
      bank1: {
        "bank-account": "UA113052990000026007036003840",
        bank: 'АТ КБ "ПРИВАТБАНК"',
      },
    },
    taxationSystem: "Платник єдиного податку",
    PDV: "без реєстрації ПДВ",
    IPN: "",
    passport: {
      status: "",
      number: "",
      date: "",
      organName: "",
      organRegion: "",
    },
  },
  "Страхоцінська Вікторія Вікторівна": {
    fullName: "Фізична особа-підприємець Страхоцінська Вікторія Вікторівна",
    shortName: "ФОП Страхоцінська Вікторія Вікторівна",
    address:
      "Україна, 32400, Хмельницька обл., Кам'янець-Подільський р-н, м.Дунаївці, вул. Молодіжна, буд. 5",
    director: "Страхоцінська Вікторія Вікторівна",
    code: "3838503660",
    fullLegalForm: "Фізична особа-підприємець",
    shortLegalForm: "ФОП",
    contacts: {
      phone: "(03858) 9-62-45",
      additionalPhone: "",
      fax: "",
      email: "",
      webSite: "",
      otherContacts: "",
    },
    signer: {
      name: "Страхоцінська Вікторія Вікторівна",
      role: "Керівник",
    },
    basisDocument: "Виписка з ЄДР",
    bankDetails: {
      bank1: {
        "bank-account": "UA623052990000026009006009775",
        bank: 'АТ КБ "ПРИВАТБАНК"',
      },
    },
    taxationSystem: "Платник єдиного податку",
    PDV: "без реєстрації ПДВ",
    IPN: "",
    passport: {
      status: "",
      number: "",
      date: "",
      organName: "",
      organRegion: "",
    },
  },
};

//========= Список договорів ============//
export const documentsList: DocumentsListType = {
  Договір_ковбаси: "Договір поставки м'ясопродуктів",
  Договір_нафтопродукти: "Договір поставки нафтопродуктів",
  Договір_свині: "Договір поставки свиней, ВРХ",
  Договір_послуги: "Договір про надання послуг",
};
