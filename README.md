# Опис додатку:

- Цей додаток створювався для використання в роботі юриста ТОВ "Верест".
- Його метою була автоматизація рутинної роботи по складанню договорів, а саме тих договорів, які складалися по шаблону.
- Додаток буде дуже корисний юристам/бухгалтерам, які обслуговують як одне так і безліч підприємств, оскільки, в додатку є можливість додати декілька суб'єктів господарювання, які обслуговує юрист. Наприклад, юрист ТОВ "Верест" обслуговував крім основного підприємства ще 5 ФОПів. А тому в додатку є можливість вибирати зі списку своїх суб'єктів господарювання ту особу, від якої потрібно укласти договір.

# Як додаток працює:

1. Потрібно авторизуватися
2. Потрібно ввести код ЄДРПОУ суб'єкта господарювання (надалі по тексту - "СГ"), з яким потрібно укласти договір.
3. По введеному коду ЄДРПОУ здійснюється перевірка СГ по базі даних Opendataua, після чого вся інформація виводиться на монітор. Цю інформацію можна роздрукувати.
4. Користувач у разі потреби може сформувати витяг з Opendataua, натиснувши кнопку "Сформувати витяг".
5. Користувач має можливість скласти проект договору із перевіреним СГ, натиснувши кнопку "Укласти договір".
6. На сторінці формування договору користувач може обрати, який саме договір потрібно згенерувати та обрати свого СГ, від якого потрібно укласти договір, а також розмір шрифту, який буде використаний в договорі.
7. На сторінці формування договору обов'язково потрібно заповнити тільки одне поле - "Номер договору", інша обов'язкова інформація отримується з офіційних реєстрів та заноститься в договір автоматично.
8. Також може додати різну інформацію, які потрібно занести в договір, а саме:
   -номер та дату договору;
   -порядок оплати товару (по факту, аванс, відтермінування);
   -підписанта зі сторони контрагента, документ, на підставі, якого діє підписант;
   -контакті дані контрагента (телефон та електронну адресу);
   -банківські реквізити контрагента;
   -поштову адресу контрагента;
   -паспортні дані контрагента;
   -дані про систему оподаткування контрагента.
9. Після введення усієї необхідної інформації, користувач натискає кнопку "Згенерувати договір" і договір автоматично генерується.

# Основні технології, які використані під час написання додатку:

    -React JS;
    -TypeSctript;
    -Redux toolkit;
    -Redux Thunk;
    -Axios;
    -API Opendataua (для отримання інформації про СГ);
    -Firebase from Google (для авторизації користувачів);
    -Reactrouter-dom;
    -HTML+SCSS;

# Додаткові бібліотеки, які використовувалися:

    -Classnames;
    -React-loader-spinner;
    -qrcode;
    -react-to-print;
    -інші

# Потрібно допрацювати:

Оптимізувати сайт - зайві ренедри і перерендери компонентів

# Зроблено:

<!-- Проблема: в договорах не видно інформації про: tax, passport, contacts, manager -->
<!-- Додати можливість користувачу додавати в qr-код інформацію про торгову мережу -->
<!-- Додати в проект бібліотеку для роботи з формами замість стейтів на кожен інпут -->
<!-- Потрібно зробити відображення реєстрації на прозоро та тендерів-->
<!-- Потрібно забрати функцію isTrue з CheckCompany.jsx-->
<!-- Виправити баг з "підписант діє" -->
<!-- Баг - в бенефеціарах якщо держава, то немає країни і адреси проживання -->
<!-- Перевірити поле courtEntity в CheckCompany.jsx -->
<!-- Перевірити поле branches в CheckCompany.jsx -->
<!-- Інформація про виконавчі провадження -->
<!-- Інформація про судові процеси-->
<!-- Інформація про судові рішення -->
<!-- Зробити окрему сторінку для судових рішень (courtDecision) -->
<!-- Коли перевіряється ФОП, то не вносяться дані про директора -->
<!-- Внести дані про Олійника і Бернашевського Я -->
<!-- Функціонал "Забули пароль?" -->
<!-- Немає галочок при виборі підписанта -->
<!-- Зберігати дані про авторизацію користувача в локальному сховищі -->
<!-- Добавити функціонал виходу з аккаунту -->
<!-- Коли добавляється банківський рахунок, то зявляється поле для ще одного рахунку -->
<!-- При відкритті домашньої сторінки треба спочатку перевіряти чи є дані про користувача в локальному сховищі, якщо ні, то робити запит на сервер для авторизації - HOC або middleware -->
<!-- Добавити паспорт ІД -->
<!-- БАГ _3270706889 - Банашко_ немає ризик факторів і не можна зробити договір-->
<!-- Баг _22767506 АТ "ХМЕЛЬНИЦЬКОБЛЕНЕРГО"_  -->
<!-- Посада підписанта в реквізитах - з великої літери -->
<!-- Добавити можливість вибирати систему оподаткування, якщо її немає в реєстрі -->
<!-- Добавити знак - авторське право -->
<!-- Добавити можливість прописувати строки оплати -->
<!-- Обробити помилку сервера -->
<!-- Зробити кнопку чи посилання на завантаження pdf-витягу з Опендатабот -->
<!-- Добавити стрілку "Назад" -->
<!-- Перевірити всі дані, які приходять з серверу. Особливо звернути увагу на податкову інформацію -->
<!-- Добавити номери сторінок при друці договору -->
<!-- Підписи сторін на кожній сторінці -->
<!-- Додати можливість змінювати розмір шрифту в договорі -->
<!-- Не очищуються паспортні дані при перевірці нового контрагента для укладення договору -->
<!-- Не вносяться паспортні дані ІД-картки через те, що не зазначена область органу, який видав паспорт -->
<!-- Зробити оновлення редаксу тільки по натисканню кнопки "Згенерувати документ" -->
<!-- Виправити помилки з повторним внесенням інформації в договір -->
<!-- Адаптувати сайт під мобільні телефони -->
<!-- Виправити помилки з відобреженням інформації про систему оподаткування -->
<!-- Контактні дані з реєстру + можливість добавити свої -->
